﻿using System;
using System.Reflection;
using Billing.Domain.Common;
using Newtonsoft.Json;

namespace Billing.Domain.Serialization.NewtonsoftJson {
    /// <summary>
    /// Конвертирует <see cref="Enumeration"/> в JSON и обратно
    /// </summary>
    public class EnumerationJsonConverter : JsonConverter<Enumeration> {
        /// <inheritdoc />
        public override void WriteJson(JsonWriter writer, Enumeration value, JsonSerializer serializer) {
            if (value is null) {
                writer.WriteNull();
            } else {
                writer.WriteValue(value.Name);
            }
        }

        /// <inheritdoc />
        public override Enumeration ReadJson(JsonReader reader,
                                             Type objectType,
                                             Enumeration existingValue,
                                             bool hasExistingValue,
                                             JsonSerializer serializer) {
            return reader.TokenType switch {
                JsonToken.Integer => GetEnumerationFromJson(reader.Value.ToString(), objectType),
                JsonToken.String => GetEnumerationFromJson(reader.Value.ToString(), objectType),
                JsonToken.Null => null,
                _ => throw new JsonSerializationException($"Unexpected token {reader.TokenType} when parsing an enumeration")
            };
        }

        private static Enumeration GetEnumerationFromJson(string nameOrValue, Type objectType) {
            try {
                MethodInfo methodInfo = typeof(Enumeration).GetMethod(
                    nameof(Enumeration.TryParse), BindingFlags.Static | BindingFlags.Public);

                if (methodInfo == null) {
                    throw new JsonSerializationException("Serialization is not supported");
                }

                MethodInfo genericMethod = methodInfo.MakeGenericMethod(objectType);

                object[] arguments = new[] { nameOrValue, default(object) };

                genericMethod.Invoke(null, arguments);
                return arguments[1] as Enumeration;
            } catch (Exception ex) {
                throw new JsonSerializationException($"Error converting value '{nameOrValue}' to a enumeration.", ex);
            }
        }
    }
}