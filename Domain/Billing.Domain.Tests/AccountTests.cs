﻿using System;
using Billing.Domain.Accounts;
using Billing.Domain.Accounts.Contracts;
using Billing.Domain.Accounts.Exceptions;
using Billing.Domain.Amounts;
using Billing.Domain.DateTimes;
using Billing.Domain.Transactions;
using Billing.Domain.Users;
using Billing.Domain.Users.Exceptions;
using Xunit;

namespace Billing.Domain.Tests {
    public class AccountTests {
        /// <summary>
        /// Проверка начальных свойств счета
        /// </summary>
        [Fact]
        internal void InitAccountTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = user.AddAccount(new CreateAccountParams {
                Currency = Currency.RUB,
                Description = new AccountDescription("test"),
                Now = DateTime.UtcNow.ToTime()
            });

            Assert.Equal(AccountStatus.Active, account.Status);
            Assert.Equal(0, account.Balance.FullBalance);
            Assert.Equal(0, account.Balance.Balance.Value);
            Assert.Equal(0, account.Balance.Reserve.Value);
            Assert.Equal(0, account.Overdraft.Value);
            Assert.Equal(Currency.RUB, account.Currency);
            Assert.NotNull(account.ActivatedTime);
            Assert.Null(account.FrozenTime);
            Assert.Null(account.ClosedTime);
            Assert.NotNull(account.Description);
            Assert.NotNull(account.CurrentPeriod);
        }

        /// <summary>
        /// Проверка заморозки счета
        /// </summary>
        [Fact]
        internal void FreezeAccountTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);

            account.Freeze(DateTime.UtcNow.ToTime());

            Assert.Equal(AccountStatus.Frozen, account.Status);
            Assert.NotNull(account.FrozenTime);
        }

        /// <summary>
        /// Проверка закрытия счета
        /// </summary>
        [Fact]
        internal void CloseAccountTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);

            account.Close(DateTime.UtcNow.ToTime());

            Assert.Equal(AccountStatus.Closed, account.Status);
            Assert.NotNull(account.ClosedTime);
        }

        /// <summary>
        /// Проверка активации счета
        /// </summary>
        [Fact]
        internal void ActivateAccountTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            account.Freeze(DateTime.UtcNow.ToTime());

            Time oldActivationTime = account.ActivatedTime;
            account.Activate(DateTime.UtcNow.ToTime());

            Assert.Equal(AccountStatus.Active, account.Status);
            Assert.NotNull(account.ActivatedTime);
            Assert.NotEqual(oldActivationTime, account.ActivatedTime);
            Assert.NotNull(account.CurrentPeriod);
        }

        /// <summary>
        /// Проверка  закрытия счета с положительным балансом
        /// </summary>
        [Fact]
        internal void CloseAccountWithPositiveBalanceTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            MockFactory.AcceptRefill(user, account, 100, DateTime.UtcNow);

            Assert.Throws<BalanceIsNonZeroException>(() => account.Close(DateTime.UtcNow.ToTime()));
        }

        /// <summary>
        /// Проверка закрытия счета с отрицательным балансом
        /// </summary>
        [Fact]
        internal void CloseAccountWithNegativeBalanceTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);

            account.SetOverdraft(new AccountOverdraft(100));
            MockFactory.AcceptWriteOff(user, account, -100, DateTime.UtcNow);

            Assert.Throws<BalanceIsNonZeroException>(() => account.Close(DateTime.UtcNow.ToTime()));
        }

        /// <summary>
        /// Проверка активации/заморозки закрытого счета
        /// </summary>
        [Fact]
        internal void ClosedAccountTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            account.Close(DateTime.UtcNow.ToTime());

            Assert.Throws<AccountIsClosedException>(() => account.Activate(DateTime.UtcNow.ToTime()));
            Assert.Throws<AccountIsClosedException>(() => account.Freeze(DateTime.UtcNow.ToTime()));
        }

        /// <summary>
        /// Проверка установки овердрафта
        /// </summary>
        [Fact]
        internal void SetOverdraftTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);

            account.SetOverdraft(new AccountOverdraft(1000));
            account.SetOverdraft(new AccountOverdraft(0));
            Assert.Throws<ArgumentException>(() => account.SetOverdraft(new AccountOverdraft(-500)));

            account.Freeze(DateTime.UtcNow.ToTime());
            Assert.Throws<AccountIsFrozenException>(() => account.SetOverdraft(new AccountOverdraft(100)));

            account.Close(DateTime.UtcNow.ToTime());
            Assert.Throws<AccountIsClosedException>(() => account.SetOverdraft(new AccountOverdraft(100)));
        }

        /// <summary>
        /// Проверка установки овердрафта ниже суммы задолженности
        /// </summary>
        [Fact]
        internal void SetOverdraftLessThanDebtTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);

            account.SetOverdraft(new AccountOverdraft(1000));
            MockFactory.AcceptWriteOff(user, account, -800, DateTime.UtcNow);
            Assert.Throws<OverdraftLessThanDebtException>(() => account.SetOverdraft(new AccountOverdraft(500)));
            account.SetOverdraft(new AccountOverdraft(800));
        }

        /// <summary>
        /// Проверка максимальной суммы баланса
        /// </summary>
        [Fact]
        internal void MaxBalanceAmountTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);

            MockFactory.AcceptRefill(user, account, Account.BALANCE_LIMIT - 1000, DateTime.UtcNow);

            Assert.Throws<BalanceLimitException>(() => MockFactory.AcceptRefill(user, account, 2000, DateTime.UtcNow));

            MockFactory.AcceptRefill(user, account, 1000, DateTime.UtcNow);
        }

        /// <summary>
        /// Проверка минимальной суммы баланса
        /// </summary>
        [Fact]
        internal void MinBalanceAmountTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);

            Transaction transaction1 = MockFactory.CreateWriteOff(user, account, -1000, DateTime.UtcNow);
            Assert.Throws<InsufficientBalanceException>(() => transaction1.Accept(DateTime.UtcNow.ToTime()));

            Assert.Throws<InsufficientBalanceException>(() => MockFactory.ReserveWriteOff(user, account, -1000, DateTime.UtcNow));

            account.SetOverdraft(new AccountOverdraft(900));
            Assert.Throws<InsufficientBalanceException>(() => transaction1.Accept(DateTime.UtcNow.ToTime()));

            account.SetOverdraft(new AccountOverdraft(1000));
            transaction1.Accept(DateTime.UtcNow.ToTime());

            Transaction transaction3 = MockFactory.CreateWriteOff(user, account, -100, DateTime.UtcNow);
            Assert.Throws<InsufficientBalanceException>(() => transaction3.Accept(DateTime.UtcNow.ToTime()));

            Assert.Throws<InsufficientBalanceException>(() => MockFactory.ReserveWriteOff(user, account, -100, DateTime.UtcNow));
        }

        /// <summary>
        /// Проверка выполнения операций над счетами заблокированного пользователя
        /// </summary>
        [Fact]
        internal void BlockedUserOperationsTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            MockFactory.AcceptRefill(user, account, 1000, DateTime.UtcNow);

            Transaction refill = MockFactory.CreateRefill(user, account, 100, DateTime.UtcNow);
            Transaction reserve = MockFactory.CreateWriteOff(user, account, -200, DateTime.UtcNow);
            Transaction accept = MockFactory.CreateWriteOff(user, account, -300, DateTime.UtcNow);

            user.Block(DateTime.UtcNow.ToTime());
            Assert.Throws<UserIsBlockedException>(() => refill.Accept(DateTime.UtcNow.ToTime()));
            Assert.Throws<UserIsBlockedException>(() => reserve.Reserve(DateTime.UtcNow.ToTime()));
            Assert.Throws<UserIsBlockedException>(() => accept.Accept(DateTime.UtcNow.ToTime()));
            Assert.Throws<UserIsBlockedException>(() => MockFactory.CreateRefill(user, account, 1000, DateTime.UtcNow));
            Assert.Throws<UserIsBlockedException>(() => MockFactory.CreateWriteOff(user, account, -1000, DateTime.UtcNow));
        }

        /// <summary>
        /// Проверка изменения баланса счета
        /// </summary>
        [Fact]
        internal void AccountBalanceTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);

            // пополнение увеличивает сумму баланса
            decimal refillAmount = 1000;
            Transaction refill = MockFactory.CreateRefill(user, account, refillAmount, DateTime.UtcNow);
            Assert.Equal(0, account.Balance.FullBalance);
            refill.Accept(DateTime.UtcNow.ToTime());
            Assert.Equal(refillAmount, account.Balance.FullBalance);

            // отмена пополнения не влияет на баланс
            MockFactory.CancelRefill(user, account, 2000, DateTime.UtcNow);
            Assert.Equal(refillAmount, account.Balance.FullBalance);

            // списание уменьшает сумму баланса
            decimal writeOffAmount = 500;
            Transaction writeOff = MockFactory.CreateWriteOff(user, account, -writeOffAmount, DateTime.UtcNow);
            Assert.Equal(refillAmount, account.Balance.FullBalance);
            writeOff.Accept(DateTime.UtcNow.ToTime());
            Assert.Equal(refillAmount - writeOffAmount, account.Balance.FullBalance);

            // отмена списания не влияет на баланс
            MockFactory.CancelWriteOff(user, account, -writeOffAmount, DateTime.UtcNow);
            Assert.Equal(refillAmount - writeOffAmount, account.Balance.FullBalance);

            // резервирование уменьшает сумму баланса и увеличивает сумму резерва
            decimal reserveAmount = 200;
            Transaction reserve = MockFactory.CreateWriteOff(user, account, -reserveAmount, DateTime.UtcNow);
            Assert.Equal(0, account.Balance.Reserve.Value);
            reserve.Reserve(DateTime.UtcNow.ToTime());
            Assert.Equal(refillAmount - writeOffAmount - reserveAmount, account.Balance.FullBalance);
            Assert.Equal(reserveAmount, account.Balance.Reserve.Value);
        }

        /// <summary>
        /// Проверка перевода между счетами
        /// </summary>
        [Fact]
        internal void TransferTest() {
            // IDateTimeProvider dateTimeProvider = new DateTimeProvider();
            // User user = MockFactory.CreateUser(DateTime.UtcNow);
            // Account fromAccount = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            // MockFactory.AcceptRefill(user, fromAccount, 1000, DateTime.UtcNow);
            // Account toAccount = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            //
            // CreateTransactionParams transferParams = new CreateTransactionParams {
            //     Amount = new Amount(300),
            //     Type = new TransactionType("transfer"),
            //     Description = new TransactionDescription("тестовый перевод")
            // };
            // transactionsManager.Transfer(user, fromAccount, toAccount, transferParams);
            //
            // Assert.Equal(1000 - 300, fromAccount.Balance.Balance.Value);
            // Assert.Equal(1000 - 300, fromAccount.Balance.FullBalance);
            // Assert.Equal(300, toAccount.Balance.Balance.Value);
            // Assert.Equal(300, toAccount.Balance.FullBalance);
            //
            // Account usdAccount = MockFactory.CreateAccount(user, Currency.USD, DateTime.UtcNow);
            // Assert.Throws<CurrencyMismatchException>(() =>
            //     transactionsManager.Transfer(user, fromAccount, usdAccount, transferParams));
            //
            // User anotherUser = MockFactory.CreateUser(DateTime.UtcNow);
            // Account anotherUserAccount = MockFactory.CreateAccount(anotherUser, Currency.RUB, DateTime.UtcNow);
            // Assert.Throws<IncorrectUserException>(() =>
            //     transactionsManager.Transfer(user, fromAccount, anotherUserAccount, transferParams));
            //
            // user.Block(DateTime.UtcNow);
            // Assert.Throws<UserIsBlockedException>(() =>
            //     transactionsManager.Transfer(user, fromAccount, toAccount, transferParams));
            // user.Activate(DateTime.UtcNow);
            //
            // fromAccount.Freeze(DateTime.UtcNow);
            // Assert.Throws<AccountIsFrozenException>(() =>
            //     transactionsManager.Transfer(user, fromAccount, toAccount, transferParams));
            // fromAccount.Activate(DateTime.UtcNow);
            //
            // toAccount.Freeze(DateTime.UtcNow);
            // Assert.Throws<AccountIsFrozenException>(() =>
            //     transactionsManager.Transfer(user, fromAccount, toAccount, transferParams));
            //
            // toAccount.Activate(DateTime.UtcNow);
            // MockFactory.AcceptWriteOff(user, toAccount, -300, DateTime.UtcNow);
            // toAccount.Close(DateTime.UtcNow);
            // Assert.Throws<AccountIsClosedException>(() =>
            //     transactionsManager.Transfer(user, fromAccount, toAccount, transferParams));
        }

        /// <summary>
        /// Проверка баланса с закрытием периода
        /// </summary>
        [Fact]
        internal void ClosingPeriodBalanceTest() {
            DateTime TwoMonthAgo() => DateTime.UtcNow.AddMonths(-2);
            User user = MockFactory.CreateUser(TwoMonthAgo());
            Account account = MockFactory.CreateAccount(user, Currency.RUB, TwoMonthAgo());
            MockFactory.AcceptRefill(user, account, 1000, TwoMonthAgo());
            MockFactory.AcceptWriteOff(user, account, -100, TwoMonthAgo());
            MockFactory.CreateRefill(user, account, 500, TwoMonthAgo());
            MockFactory.CreateWriteOff(user, account, -100, TwoMonthAgo());
            MockFactory.ReserveWriteOff(user, account, -200, TwoMonthAgo());
            MockFactory.CancelRefill(user, account, 200, TwoMonthAgo());
            MockFactory.CancelWriteOff(user, account, -300, TwoMonthAgo());

            Assert.Equal(700, account.Balance.FullBalance);

            DateTime OneMonthAgo() => DateTime.UtcNow.AddMonths(-1);
            MockFactory.AcceptWriteOff(user, account, -200, OneMonthAgo());

            Assert.Equal(500, account.Balance.FullBalance);

            account.CurrentPeriod.ClosePeriod(OneMonthAgo().ToTime());

            Assert.Equal(500, account.Balance.FullBalance);

            account.CurrentPeriod.ClosePeriod(DateTime.UtcNow.ToTime());

            Assert.Equal(500, account.Balance.FullBalance);

            MockFactory.AcceptWriteOff(user, account, -200, DateTime.UtcNow);

            Assert.Equal(300, account.Balance.FullBalance);
        }
    }
}