using System;
using Billing.Domain.DateTimes;
using Billing.Domain.Users;
using Xunit;

namespace Billing.Domain.Tests {
    public class UserTests {
        /// <summary>
        /// Проверка начальных свойств пользователя при создании
        /// </summary>
        [Fact]
        internal void InitUserTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);

            Assert.Equal(UserStatus.Active, user.Status);
            Assert.Equal(user.CreatedTime, user.ActivatedTime);
            Assert.Null(user.BlockedTime);
            Assert.NotNull(user.Name);
            Assert.NotNull(user.ExternalUser?.Product?.Value);
            Assert.NotNull(user.ExternalUser?.ExternalId?.Value);
            Assert.NotNull(user.Accounts);
            Assert.Equal(0, user.Accounts.Count);
        }

        /// <summary>
        /// Проверка блокировки пользователя
        /// </summary>
        [Fact]
        internal void BlockUserTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);

            user.Block(DateTime.UtcNow.ToTime());

            Assert.Equal(UserStatus.Blocked, user.Status);
            Assert.NotNull(user.BlockedTime);
        }

        /// <summary>
        /// Проверка активации пользователя
        /// </summary>
        [Fact]
        internal void ActivateUserTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            user.Block(DateTime.UtcNow.ToTime());

            Time oldActivationTime = user.ActivatedTime;
            user.Activate(DateTime.UtcNow.ToTime());

            Assert.Equal(UserStatus.Active, user.Status);
            Assert.NotNull(user.ActivatedTime);
            Assert.NotEqual(oldActivationTime, user.ActivatedTime);
        }
    }
}