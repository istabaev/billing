﻿using System;
using Billing.Domain.Accounts;
using Billing.Domain.Amounts;
using Billing.Domain.DateTimes;
using Billing.Domain.Periods;
using Billing.Domain.Periods.Exceptions;
using Billing.Domain.Users;
using Xunit;

namespace Billing.Domain.Tests {
    public class PeriodTests {
        /// <summary>
        /// Проверка начальных свойств транзакции
        /// </summary>
        [Fact]
        internal void InitPeriodTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            Period period = account.CurrentPeriod;

            Assert.Equal(PeriodStatus.Active, period.Status);
            Assert.Equal(account.Balance.FullBalance, period.StartBalance.Balance.Value);
            Assert.NotNull(period.Transactions);
            Assert.Equal(0, period.Transactions.Count);
        }

        /// <summary>
        /// Проверка закрытия периода
        /// </summary>
        [Fact]
        internal void ClosingTest() {
            DateTime OneMonthAgo() => DateTime.UtcNow.AddMonths(-1);
            User user = MockFactory.CreateUser(OneMonthAgo());
            Account account = MockFactory.CreateAccount(user, Currency.RUB, OneMonthAgo());
            Period firstPeriod = account.CurrentPeriod;
            MockFactory.AcceptRefill(user, account, 1000, OneMonthAgo());

            Assert.Equal(DateTimeHelper.GetMonthBeginning(DateTime.UtcNow.AddMonths(-1)), firstPeriod.StartDate);
            Assert.Throws<IncorrectPeriodDatesException>(() => account.CurrentPeriod.ClosePeriod(OneMonthAgo().ToTime()));

            account.CurrentPeriod.ClosePeriod(DateTime.UtcNow.ToTime());
            Period secondPeriod = account.CurrentPeriod;

            Assert.Equal(PeriodStatus.Closed, firstPeriod.Status);
            Assert.NotNull(firstPeriod.ClosedTime);
            Assert.NotNull(firstPeriod.EndDate);
            Assert.Equal(DateTimeHelper.GetMonthBeginning(DateTime.UtcNow), firstPeriod.EndDate);
            Assert.Equal(firstPeriod.StartDate.Value.AddMonths(1), firstPeriod.EndDate.Value);
            Assert.NotNull(firstPeriod.EndBalance);
            Assert.Equal(1000, firstPeriod.EndBalance.Balance.Value);
            Assert.NotEqual(secondPeriod, firstPeriod);
            Assert.Equal(firstPeriod.EndDate.Value, secondPeriod.StartDate.Value);
            Assert.Equal(1000, secondPeriod.StartBalance.Balance.Value);
        }

        /// <summary>
        /// Проверка закрытия периода для закрытого счета
        /// </summary>
        [Fact]
        internal void ClosedAccountTest() {
            DateTime TwoMonthAgo() => DateTime.UtcNow.AddMonths(-2);
            User user = MockFactory.CreateUser(TwoMonthAgo());
            Account account = MockFactory.CreateAccount(user, Currency.RUB, TwoMonthAgo());
            Period period = account.CurrentPeriod;
            account.Close(TwoMonthAgo().ToTime());

            DateTime OneMonthAgo() => DateTime.UtcNow.AddMonths(-1);
            account.CurrentPeriod.ClosePeriod(OneMonthAgo().ToTime());

            Assert.Equal(period, account.CurrentPeriod);

            Assert.Throws<PeriodIsClosedException>(() => account.CurrentPeriod.ClosePeriod(DateTime.UtcNow.ToTime()));
        }

        /// <summary>
        /// Проверка перехода транзакций в следующий период после закрытия
        /// </summary>
        [Fact]
        internal void NotFinalTransactionsMigrationTest() {
            DateTime OneMonthAgo() => DateTime.UtcNow.AddMonths(-1);
            User user = MockFactory.CreateUser(OneMonthAgo());
            Account account = MockFactory.CreateAccount(user, Currency.RUB, OneMonthAgo());
            Period firstPeriod = account.CurrentPeriod;
            MockFactory.AcceptRefill(user, account, 1000, OneMonthAgo());
            MockFactory.CreateRefill(user, account, 500, OneMonthAgo());
            MockFactory.CreateWriteOff(user, account, -100, OneMonthAgo());
            MockFactory.ReserveWriteOff(user, account, -200, OneMonthAgo());
            MockFactory.CancelRefill(user, account, 200, OneMonthAgo());
            MockFactory.CancelWriteOff(user, account, -300, OneMonthAgo());
            MockFactory.AcceptWriteOff(user, account, -400, OneMonthAgo());

            Assert.Equal(7, firstPeriod.Transactions.Count);

            account.CurrentPeriod.ClosePeriod(DateTime.UtcNow.ToTime());
            Period secondPeriod = account.CurrentPeriod;

            Assert.Equal(4, firstPeriod.Transactions.Count);
            Assert.Equal(3, secondPeriod.Transactions.Count);
        }

        /// <summary>
        /// Проверка перехода транзакций в следующий период после закрытия с задержкой
        /// </summary>
        [Fact]
        internal void NotFinalTransactionsMigrationWithDelayTest() {
            DateTime OneMonthAgo() => DateTime.UtcNow.AddMonths(-1);
            User user = MockFactory.CreateUser(OneMonthAgo());
            Account account = MockFactory.CreateAccount(user, Currency.RUB, OneMonthAgo());
            Period firstPeriod = account.CurrentPeriod;
            MockFactory.AcceptRefill(user, account, 1000, OneMonthAgo());
            MockFactory.CreateRefill(user, account, 500, OneMonthAgo());
            MockFactory.CreateWriteOff(user, account, -100, OneMonthAgo());
            MockFactory.ReserveWriteOff(user, account, -200, OneMonthAgo());
            MockFactory.CancelRefill(user, account, 200, OneMonthAgo());
            MockFactory.CancelWriteOff(user, account, -300, OneMonthAgo());
            MockFactory.AcceptWriteOff(user, account, -400, OneMonthAgo());

            Assert.Equal(7, firstPeriod.Transactions.Count);

            MockFactory.CreateRefill(user, account, 200, DateTime.UtcNow);
            MockFactory.CreateWriteOff(user, account, -200, DateTime.UtcNow);
            MockFactory.ReserveWriteOff(user, account, -100, DateTime.UtcNow);
            MockFactory.CancelRefill(user, account, 300, DateTime.UtcNow);
            MockFactory.CancelWriteOff(user, account, -300, DateTime.UtcNow);
            MockFactory.AcceptRefill(user, account, 400, DateTime.UtcNow);
            MockFactory.AcceptWriteOff(user, account, -400, DateTime.UtcNow);

            Assert.Equal(14, firstPeriod.Transactions.Count);

            account.CurrentPeriod.ClosePeriod(DateTime.UtcNow.ToTime());
            Period secondPeriod = account.CurrentPeriod;

            Assert.Equal(4, firstPeriod.Transactions.Count);
            Assert.Equal(10, secondPeriod.Transactions.Count);
        }
    }
}