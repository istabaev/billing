﻿using System;
using Billing.Domain.Accounts;
using Billing.Domain.Amounts;
using Billing.Domain.DateTimes;
using Billing.Domain.Periods.Exceptions;
using Billing.Domain.Transactions;
using Billing.Domain.Transactions.Exceptions;
using Billing.Domain.Users;
using Xunit;

namespace Billing.Domain.Tests {
    public class TransactionTests {
        /// <summary>
        /// Проверка начальных свойств транзакции
        /// </summary>
        [Fact]
        internal void InitTransactionTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            Transaction transaction = MockFactory.CreateRefill(user, account, 100, DateTime.UtcNow);

            Assert.Equal(100, transaction.Amount.Value);
            Assert.Equal(account.Currency, transaction.Currency);
            Assert.Equal(TransactionStatus.Created, transaction.Status);
            Assert.Null(transaction.Date);
            Assert.Null(transaction.ReservedTime);
            Assert.Null(transaction.CancelledTime);
            Assert.Null(transaction.AcceptedTime);
            Assert.NotNull(transaction.Type);
            Assert.NotNull(transaction.Description);
        }

        /// <summary>
        /// Проверка добавления транзакции с нулевой суммой
        /// </summary>
        [Fact]
        internal void ZeroAmountTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);

            Assert.Throws<ArgumentException>(() => MockFactory.CreateRefill(user, account, 0, DateTime.UtcNow));
        }

        /// <summary>
        /// Проверка резервирования транзакции пополнения
        /// </summary>
        [Fact]
        internal void ReserveRefillTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            Transaction transaction = MockFactory.CreateRefill(user, account, 1000, DateTime.UtcNow);

            Assert.Throws<IncorrectTransactionStatusException>(() => transaction.Reserve(DateTime.UtcNow.ToTime()));
        }

        /// <summary>
        /// Проверка резервирования транзакции
        /// </summary>
        [Fact]
        internal void ReserveTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            MockFactory.AcceptRefill(user, account, 1000, DateTime.UtcNow);

            Transaction transaction = MockFactory.ReserveWriteOff(user, account, -500, DateTime.UtcNow);

            Assert.Equal(TransactionStatus.Reserved, transaction.Status);
            Assert.NotNull(transaction.ReservedTime);
        }

        /// <summary>
        /// Проверка резервирования транзакции
        /// </summary>
        [Fact]
        internal void AcceptTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);

            Transaction refill = MockFactory.CreateRefill(user, account, 1000, DateTime.UtcNow);
            Assert.Null(refill.Date);
            refill.Accept(DateTime.UtcNow.ToTime());

            Assert.Equal(TransactionStatus.Accepted, refill.Status);
            Assert.NotNull(refill.AcceptedTime);
            Assert.NotNull(refill.Date);
            Assert.Equal(refill.AcceptedTime.Value.Date, refill.Date.Value.Date);

            Transaction writeOff = MockFactory.CreateWriteOff(user, account, -500, DateTime.UtcNow);
            Assert.Null(writeOff.Date);
            writeOff.Accept(DateTime.UtcNow.ToTime());

            Assert.Equal(TransactionStatus.Accepted, writeOff.Status);
            Assert.NotNull(writeOff.AcceptedTime);
            Assert.NotNull(writeOff.Date);
            Assert.Equal(writeOff.AcceptedTime.Value.Date, writeOff.Date.Value.Date);
        }

        /// <summary>
        /// Проверка отмены транзакции
        /// </summary>
        [Fact]
        internal void CancelTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            MockFactory.AcceptRefill(user, account, 1000, DateTime.UtcNow);

            Transaction refill = MockFactory.CancelRefill(user, account, 1000, DateTime.UtcNow);

            Assert.Equal(TransactionStatus.Cancelled, refill.Status);
            Assert.NotNull(refill.CancelledTime);

            Transaction writeOff = MockFactory.CancelWriteOff(user, account, -500, DateTime.UtcNow);

            Assert.Equal(TransactionStatus.Cancelled, writeOff.Status);
            Assert.NotNull(writeOff.CancelledTime);
        }

        /// <summary>
        /// Проверка изменения статуса подтвержденной транзакции
        /// </summary>
        [Fact]
        internal void AcceptedTransactionTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            MockFactory.AcceptRefill(user, account, 1000, DateTime.UtcNow);

            Transaction refill = MockFactory.AcceptRefill(user, account, 100, DateTime.UtcNow);

            Assert.Throws<TransactionIsAcceptedException>(() => refill.Reserve(DateTime.UtcNow.ToTime()));
            Assert.Throws<TransactionIsAcceptedException>(() => refill.Accept(DateTime.UtcNow.ToTime()));
            Assert.Throws<TransactionIsAcceptedException>(() => refill.Cancel(DateTime.UtcNow.ToTime()));

            Transaction writeOff = MockFactory.AcceptWriteOff(user, account, -100, DateTime.UtcNow);

            Assert.Throws<TransactionIsAcceptedException>(() => writeOff.Reserve(DateTime.UtcNow.ToTime()));
            Assert.Throws<TransactionIsAcceptedException>(() => writeOff.Accept(DateTime.UtcNow.ToTime()));
            Assert.Throws<TransactionIsAcceptedException>(() => writeOff.Cancel(DateTime.UtcNow.ToTime()));
        }

        /// <summary>
        /// Проверка изменения статуса отмененной транзакции
        /// </summary>
        [Fact]
        internal void CancelledTransactionTest() {
            User user = MockFactory.CreateUser(DateTime.UtcNow);
            Account account = MockFactory.CreateAccount(user, Currency.RUB, DateTime.UtcNow);
            MockFactory.AcceptRefill(user, account, 1000, DateTime.UtcNow);

            Transaction refill = MockFactory.CancelRefill(user, account, 100, DateTime.UtcNow);

            Assert.Throws<TransactionIsCancelledException>(() => refill.Reserve(DateTime.UtcNow.ToTime()));
            Assert.Throws<TransactionIsCancelledException>(() => refill.Accept(DateTime.UtcNow.ToTime()));
            Assert.Throws<TransactionIsCancelledException>(() => refill.Cancel(DateTime.UtcNow.ToTime()));

            Transaction writeOff = MockFactory.CancelWriteOff(user, account, -100, DateTime.UtcNow);

            Assert.Throws<TransactionIsCancelledException>(() => writeOff.Reserve(DateTime.UtcNow.ToTime()));
            Assert.Throws<TransactionIsCancelledException>(() => writeOff.Accept(DateTime.UtcNow.ToTime()));
            Assert.Throws<TransactionIsCancelledException>(() => writeOff.Cancel(DateTime.UtcNow.ToTime()));
        }

        /// <summary>
        /// Проверка изменения подтвержденной транзакции в закрытом периоде
        /// </summary>
        [Fact]
        internal void ClosedPeriodTest() {
            DateTime OneMonthAgo() => DateTime.UtcNow.AddMonths(-1);
            User user = MockFactory.CreateUser(OneMonthAgo());
            Account account = MockFactory.CreateAccount(user, Currency.RUB, OneMonthAgo());
            MockFactory.AcceptRefill(user, account, 1000, OneMonthAgo());
            MockFactory.AcceptWriteOff(user, account, -100, OneMonthAgo());
            Transaction refill = MockFactory.CreateRefill(user, account, 500, OneMonthAgo());
            Transaction writeOff = MockFactory.CreateWriteOff(user, account, -500, OneMonthAgo());

            account.CurrentPeriod.ClosePeriod(DateTime.UtcNow.ToTime());

            Assert.Throws<PeriodIsClosedException>(() =>
                refill.Accept(DateTime.UtcNow.ToTime(), OneMonthAgo().ToDate()));
            Assert.Throws<PeriodIsClosedException>(() =>
                writeOff.Accept(DateTime.UtcNow.ToTime(), OneMonthAgo().ToDate()));

            Transaction refill2 = MockFactory.CreateRefill(user, account, 500, DateTime.UtcNow);
            Transaction writeOff2 = MockFactory.CreateWriteOff(user, account, -200, DateTime.UtcNow);
            Assert.Throws<DateInClosedPeriodException>(() =>
                refill2.Accept(DateTime.UtcNow.ToTime(), OneMonthAgo().ToDate()));
            Assert.Throws<DateInClosedPeriodException>(() =>
                writeOff2.Accept(DateTime.UtcNow.ToTime(), OneMonthAgo().ToDate()));
        }
    }
}