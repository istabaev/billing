﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Billing.Domain.Accounts;
using Billing.Domain.Accounts.Contracts;
using Billing.Domain.Amounts;
using Billing.Domain.Common;
using Billing.Domain.DateTimes;
using Billing.Domain.Periods;
using Billing.Domain.Transactions;
using Billing.Domain.Transactions.Contracts;
using Billing.Domain.Users;

namespace Billing.Domain.Tests {
    /// <summary>
    /// Фабрика сущностей
    /// </summary>
    internal static class MockFactory {
        private static readonly Dictionary<Type, long> _sequences = new Dictionary<Type, long> {
            { typeof(User), 0 },
            { typeof(Account), 0 },
            { typeof(Transaction), 0 },
            { typeof(Period), 0 }
        };

        private static void SetSequenceId(Entity<long> entity) {
            Type entityType = entity.GetType();
            if (entity.Id > 0) {
                return;
            }
            long id = ++_sequences[entityType];
            PropertyInfo propertyInfo = entityType.GetProperty(nameof(entity.Id));
            if (propertyInfo == null) throw new ArgumentNullException(nameof(propertyInfo));
            propertyInfo.SetValue(entity, id);
        }

        internal static User CreateUser(DateTime now) {
            ExternalUser externalUser = new ExternalUser(new Product("test"), new ExternalId(Guid.NewGuid().ToString("N")));
            User user = new User(externalUser, new UserName("John Doe"), new Time(now));
            SetSequenceId(user);
            return user;
        }

        internal static Account CreateAccount(User user, Currency currency, DateTime now) {
            Account account = user.AddAccount(new CreateAccountParams {
                Currency = currency,
                Description = new AccountDescription($"Test {currency:G}"),
                Now = new Time(now)
            });
            SetSequenceId(account);
            account.Activate(new Time(now));
            SaveEntity(account.CurrentPeriod);
            return account;
        }

        internal static void SaveEntity<TEntity>(TEntity entity) where TEntity : Entity<long> {
            if (entity != null) {
                SetSequenceId(entity);
            }
        }

        internal static Transaction CreateRefill(User user, Account account, decimal amount, DateTime now) {
            Transaction transaction = account.AddTransaction(new CreateTransactionParams {
                Amount = new Amount(amount),
                Type = new TransactionType("refill"),
                Description = new TransactionDescription("Тестовое пополнение"),
                Now = new Time(now)
            });
            SetSequenceId(transaction);
            return transaction;
        }

        internal static Transaction CreateWriteOff(User user, Account account, decimal amount, DateTime now) {
            Transaction transaction = account.AddTransaction(new CreateTransactionParams {
                Amount = new Amount(amount),
                Type = new TransactionType("write-off"),
                Description = new TransactionDescription("Тестовое списание"),
                Now = new Time(now)
            });
            SetSequenceId(transaction);
            return transaction;
        }

        internal static Transaction AcceptRefill(User user, Account account, decimal amount, DateTime now) {
            Transaction refill = CreateRefill(user, account, amount, now);
            refill.Accept(new Time(now));
            return refill;
        }

        internal static Transaction CancelRefill(User user, Account account, decimal amount, DateTime now) {
            Transaction refill = CreateRefill(user, account, amount, now);
            refill.Cancel(new Time(now));
            return refill;
        }

        internal static Transaction ReserveWriteOff(User user, Account account, decimal amount, DateTime now) {
            Transaction writeOff = CreateWriteOff(user, account, amount, now);
            writeOff.Reserve(new Time(now));
            return writeOff;
        }

        internal static Transaction AcceptWriteOff(User user, Account account, decimal amount, DateTime now) {
            Transaction writeOff = CreateWriteOff(user, account, amount, now);
            writeOff.Accept(new Time(now));
            return writeOff;
        }

        internal static Transaction CancelWriteOff(User user, Account account, decimal amount, DateTime now) {
            Transaction writeOff = CreateWriteOff(user, account, amount, now);
            writeOff.Cancel(new Time(now));
            return writeOff;
        }
    }
}