﻿using System;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using Billing.Domain.Common;

namespace Billing.Domain.Serialization.SystemTextJson {
    /// <summary>
    /// Конвертирует <see cref="Enumeration"/> в JSON и обратно
    /// </summary>
    public class EnumerationJsonConverter : JsonConverter<Enumeration> {
        private const string NAME_PROPERTY = nameof(Enumeration.Name);

        /// <inheritdoc />
        public override bool CanConvert(Type objectType) {
            return objectType.IsSubclassOf(typeof(Enumeration));
        }

        /// <inheritdoc />
        public override Enumeration Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            return reader.TokenType switch {
                JsonTokenType.Number => GetEnumerationFromJson(reader.GetString(), typeToConvert),
                JsonTokenType.String => GetEnumerationFromJson(reader.GetString(), typeToConvert),
                JsonTokenType.Null => null,
                _ => throw new JsonException($"Unexpected token {reader.TokenType} when parsing the enumeration.")
            };
        }

        /// <inheritdoc />
        public override void Write(Utf8JsonWriter writer, Enumeration value, JsonSerializerOptions options) {
            if (value is null) {
                writer.WriteNull(NAME_PROPERTY);
            } else {
                PropertyInfo name = value.GetType().GetProperty(NAME_PROPERTY, BindingFlags.Public | BindingFlags.Instance);
                if (name == null) {
                    throw new JsonException($"Error while writing JSON for {value}");
                }

                writer.WriteStringValue(name.GetValue(value).ToString());
            }
        }

        private static Enumeration GetEnumerationFromJson(string nameOrValue, Type objectType) {
            try {
                MethodInfo methodInfo = typeof(Enumeration).GetMethod(
                    nameof(Enumeration.TryParse), BindingFlags.Static | BindingFlags.Public);

                if (methodInfo == null) {
                    throw new JsonException("Serialization is not supported");
                }

                MethodInfo genericMethod = methodInfo.MakeGenericMethod(objectType);

                object[] arguments = new[] { nameOrValue, default(object) };

                genericMethod.Invoke(null, arguments);
                return arguments[1] as Enumeration;
            } catch (Exception ex) {
                throw new JsonException($"Error converting value '{nameOrValue}' to a enumeration.", ex);
            }
        }
    }
}