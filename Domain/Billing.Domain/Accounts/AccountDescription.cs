﻿using System;
using Billing.Domain.Common;

namespace Billing.Domain.Accounts {
    /// <summary>
    /// Описание счета
    /// </summary>
    public class AccountDescription : StringValueObject<AccountDescription> {
        public const int MAX_LENGTH = 256;

        private AccountDescription() { }

        public AccountDescription(string value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(string value) {
            return !string.IsNullOrWhiteSpace(value) && value.Length <= MAX_LENGTH;
        }
    }
}