﻿using Billing.Domain.Amounts;
using Billing.Domain.DateTimes;

namespace Billing.Domain.Accounts.Contracts {
    /// <summary>
    /// Параметры, необходимые при создании счета
    /// </summary>
    public class CreateAccountParams {
        /// <summary>
        /// Валюта
        /// </summary>
        public Currency Currency { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public AccountDescription Description { get; set; }

        /// <summary>
        /// Текущее время в UTC
        /// </summary>
        public Time Now { get; set; }
    }
}