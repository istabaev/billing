﻿using System;
using Billing.Domain.Amounts;

namespace Billing.Domain.Accounts {
    /// <summary>
    /// Сумма баланса счета
    /// </summary>
    public class Balance : Amount<Balance> {
        private Balance() { }

        public Balance(decimal value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(decimal value) {
            return true;
        }
    }
}