﻿using System;
using Billing.Domain.Amounts;

namespace Billing.Domain.Accounts {
    /// <summary>
    /// Сумма резерва счета
    /// </summary>
    public class Reserve : Amount<Reserve> {
        private Reserve() { }

        public Reserve(decimal value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(decimal value) {
            return value >= 0;
        }
    }
}