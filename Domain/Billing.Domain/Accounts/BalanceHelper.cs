﻿using System;
using System.Linq;
using Billing.Domain.Periods;
using Billing.Domain.Transactions;

namespace Billing.Domain.Accounts {
    /// <summary>
    /// Вспомогательные методы для работы с балансом счета
    /// </summary>
    internal static class BalanceHelper {
        /// <summary>
        /// Вычисляет баланс и резерв счета
        /// </summary>
        /// <param name="account">Счет</param>
        public static AccountBalance CalculateFullBalance(Account account) {
            return AccountBalance.Create(CalculateBalance(account), CalculateReserve(account));
        }

        /// <summary>
        /// Вычисляет баланс счета
        /// </summary>
        /// <param name="account">Счет</param>
        public static decimal CalculateBalance(Account account) {
            Period period = account.CurrentPeriod;
            decimal startBalance = period.StartBalance.Balance.Value;
            decimal balance = period.Transactions
                .Where(x => x.Status == TransactionStatus.Accepted)
                .Where(x => x.Date >= period.StartDate)
                .Sum(x => x.Amount.Value);
            return startBalance + balance;
        }

        /// <summary>
        /// Вычисляет баланс счета на определенную дату в текущем периоде
        /// </summary>
        /// <param name="account">Счет</param>
        /// <param name="dateTime">Дата</param>
        public static decimal CalculateBalanceOnDate(Account account, DateTime dateTime) {
            Period period = account.CurrentPeriod;
            if (dateTime < period.StartDate.Value) {
                throw new ArgumentOutOfRangeException(nameof(dateTime), dateTime, "Указанная дата раньше даты начала периода");
            }

            decimal startBalance = period.StartBalance.Balance.Value;
            decimal balanceOnDate = period.Transactions
                .Where(x => x.Status == TransactionStatus.Accepted)
                .Where(x => x.Date >= period.StartDate)
                .Where(x => x.Date < dateTime)
                .Sum(x => x.Amount.Value);
            return startBalance + balanceOnDate;
        }

        /// <summary>
        /// Вычисляет резерв счета
        /// </summary>
        /// <param name="account">Счет</param>
        public static decimal CalculateReserve(Account account) {
            Period period = account.CurrentPeriod;
            return -period.Transactions
                .Where(x => x.Status == TransactionStatus.Reserved)
                .Sum(x => x.Amount.Value);
        }

        /// <summary>
        /// Вычисляет резерв счета на определенную дату в текущем периоде
        /// </summary>
        /// <param name="account">Счет</param>
        /// <param name="dateTime">Дата</param>
        public static decimal CalculateReserveOnDate(Account account, DateTime dateTime) {
            Period period = account.CurrentPeriod;
            return -period.Transactions
                .Where(x => x.Status == TransactionStatus.Reserved)
                .Where(x => x.ReservedTime < dateTime)
                .Sum(x => x.Amount.Value);
        }
    }
}