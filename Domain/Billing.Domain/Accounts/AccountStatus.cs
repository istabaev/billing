﻿using Billing.Domain.Common;

namespace Billing.Domain.Accounts {
    /// <summary>
    /// Статус счета
    /// </summary>
    public class AccountStatus : Enumeration {
        /// <summary>
        /// Активен
        /// </summary>
        public static readonly AccountStatus Active = new AccountStatus(1, nameof(Active));

        /// <summary>
        /// Заморожен
        /// </summary>
        public static readonly AccountStatus Frozen = new AccountStatus(2, nameof(Frozen));

        /// <summary>
        /// Заблокирован
        /// </summary>
        public static readonly AccountStatus Closed = new AccountStatus(3, nameof(Closed));

        private AccountStatus() { }

        private AccountStatus(int value, string name) : base(value, name) {}
    }
}