﻿using System;
using System.Collections.Generic;
using System.Linq;
using Billing.Domain.Accounts.Exceptions;
using Billing.Domain.Amounts;
using Billing.Domain.Common;
using Billing.Domain.DateTimes;
using Billing.Domain.Periods;
using Billing.Domain.Transactions;
using Billing.Domain.Transactions.Contracts;
using Billing.Domain.Users;
using Billing.Domain.Users.Exceptions;

namespace Billing.Domain.Accounts {
    /// <summary>
    /// Счет пользователя
    /// </summary>
    public class Account : Entity<long> {
        internal const decimal BALANCE_LIMIT = decimal.MaxValue;

        private readonly List<Period> _periods = new List<Period>();

        private Account() { }

        internal Account(User user, Currency currency, AccountDescription description, Time createdTime) {
            User = user ?? throw new ArgumentNullException(nameof(user));
            UserValidator.ThrowIfUserIsBlocked(user);

            Status = AccountStatus.Active;
            Balance = AccountBalance.Create();
            Overdraft = new AccountOverdraft(0);
            Currency = currency ?? throw new ArgumentNullException(nameof(currency));
            Description = description ?? throw new ArgumentNullException(nameof(currency));
            CreatedTime = createdTime?.Clone() ?? throw new ArgumentNullException(nameof(createdTime));
            ActivatedTime = createdTime.Clone();
        }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; private set; }

        /// <summary>
        /// Статус
        /// </summary>
        public AccountStatus Status { get; internal set; }

        /// <summary>
        /// Баланс и резерв
        /// </summary>
        public AccountBalance Balance { get; internal set; }

        /// <summary>
        /// Овердрафт
        /// </summary>
        public AccountOverdraft Overdraft { get; internal set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public Currency Currency { get; private set; }

        /// <summary>
        /// Описание
        /// </summary>
        public AccountDescription Description { get; private set; }

        /// <summary>
        /// Дата и время создания
        /// </summary>
        public Time CreatedTime { get; private set; }

        /// <summary>
        /// Дата и время активации
        /// </summary>
        public Time ActivatedTime { get; internal set; }

        /// <summary>
        /// Дата и время заморозки
        /// </summary>
        public Time FrozenTime { get; internal set; }

        /// <summary>
        /// Дата и время блокировки
        /// </summary>
        public Time ClosedTime { get; internal set; }

        /// <summary>
        /// Список периодов счета
        /// </summary>
        public IReadOnlyCollection<Period> Periods => _periods.AsReadOnly();

        /// <summary>
        /// Текущий период
        /// </summary>
        public Period CurrentPeriod => _periods.OrderByDescending(x => x.CreatedTime).FirstOrDefault();

        /// <summary>
        /// Добавляет новый период на счете
        /// </summary>
        /// <param name="period">Период</param>
        internal void AddPeriod(Period period) {
            _periods.Add(period);
        }

        /// <summary>
        /// Активирует счет
        /// </summary>
        /// <param name="now">Текущее время в UTC</param>
        /// <returns><code>true</code> - если статус изменился</returns>
        /// <exception cref="UserIsBlockedException"></exception>
        /// <exception cref="AccountIsClosedException"></exception>
        public bool Activate(Time now) {
            if (now == null) throw new ArgumentNullException(nameof(now));
            UserValidator.ThrowIfUserIsBlocked(User);
            AccountValidator.ThrowIfAccountIsClosed(this);

            if (Status == AccountStatus.Active) {
                return false;
            }

            Status = AccountStatus.Active;
            ActivatedTime = now.Clone();
            return true;
        }

        /// <summary>
        /// Замораживает счет
        /// </summary>
        /// <param name="now">Текущее время в UTC</param>
        /// <returns><code>true</code> - если статус изменился</returns>
        /// <exception cref="AccountIsClosedException"></exception>
        public bool Freeze(Time now) {
            if (now == null) throw new ArgumentNullException(nameof(now));
            AccountValidator.ThrowIfAccountIsClosed(this);

            if (Status == AccountStatus.Frozen) {
                return false;
            }

            Status = AccountStatus.Frozen;
            FrozenTime = now.Clone();
            return true;
        }

        /// <summary>
        /// Закрывает счет
        /// </summary>
        /// <param name="now">Текущее время в UTC</param>
        /// <returns><code>true</code> - если статус изменился</returns>
        /// <exception cref="BalanceIsNonZeroException"></exception>
        public bool Close(Time now) {
            if (now == null) throw new ArgumentNullException(nameof(now));
            AccountValidator.ThrowIfBalanceIsNonZero(this);

            if (Status == AccountStatus.Closed) {
                return false;
            }

            Status = AccountStatus.Closed;
            ClosedTime = now.Clone();
            return true;
        }

        /// <summary>
        /// Устанавливает счету овердрафт
        /// </summary>
        /// <param name="overdraft">Сумма овердрафта</param>
        /// <exception cref="AccountIsFrozenException"></exception>
        /// <exception cref="AccountIsClosedException"></exception>
        /// <exception cref="OverdraftLessThanDebtException"></exception>
        public void SetOverdraft(AccountOverdraft overdraft) {
            if (overdraft == null) throw new ArgumentNullException(nameof(overdraft));
            AccountValidator.ThrowIfAccountIsFrozen(this);
            AccountValidator.ThrowIfAccountIsClosed(this);
            AccountValidator.ThrowIfOverdraftIsLessThanDebt(this, overdraft.Value);

            Overdraft = overdraft;
        }

        /// <summary>
        /// Добавляет новую транзакцию
        /// </summary>
        /// <param name="params">Параметры для создания новой транзакции</param>
        public Transaction AddTransaction(CreateTransactionParams @params) {
            if (@params == null) throw new ArgumentNullException(nameof(@params));
            Transaction transaction = new Transaction(User, this, CurrentPeriod, @params.Amount, Currency, @params.Type, @params.Description, @params.Now);
            CurrentPeriod.AddTransaction(transaction);
            return transaction;
        }
    }
}