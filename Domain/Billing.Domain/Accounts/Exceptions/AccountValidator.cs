﻿namespace Billing.Domain.Accounts.Exceptions {
    /// <summary>
    /// Методы валидации счета
    /// </summary>
    internal static class AccountValidator {
        /// <summary>
        /// Выбрасывает исключение, если счет заморожен
        /// </summary>
        /// <param name="account">Счет</param>
        /// <exception cref="AccountIsFrozenException"></exception>
        public static void ThrowIfAccountIsFrozen(Account account) {
            if (account.Status == AccountStatus.Frozen) throw new AccountIsFrozenException();
        }

        /// <summary>
        /// Выбрасывает исключение, если счет закрыт
        /// </summary>
        /// <param name="account">Счет</param>
        /// <exception cref="AccountIsClosedException"></exception>
        public static void ThrowIfAccountIsClosed(Account account) {
            if (account.Status == AccountStatus.Closed) throw new AccountIsClosedException();
        }

        /// <summary>
        /// Выбрасывает исключение, если баланс счета не нулевой
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <exception cref="BalanceIsNonZeroException"></exception>
        public static void ThrowIfBalanceIsNonZero(Account account) {
            if (account.Balance.FullBalance != decimal.Zero || account.Balance.Balance.Value != decimal.Zero || account.Balance.Reserve.Value != decimal.Zero) throw new BalanceIsNonZeroException();
        }

        /// <summary>
        /// Выбрасывает исключение, если баланс вышел за пределы лимита или овердрафта
        /// </summary>
        /// <param name="account">Счет</param>
        /// <param name="amount">Сумма, на которую должен измениться баланс</param>
        /// <exception cref="BalanceLimitException"></exception>
        /// <exception cref="InsufficientBalanceException"></exception>
        public static void ThrowIfBalanceIsOutOfLimit(Account account, decimal amount) {
            if (amount > 0 && account.Balance.Balance.Value > Account.BALANCE_LIMIT - amount) throw new BalanceLimitException();
            if (amount < 0 && account.Balance.FullBalance < -account.Overdraft.Value - amount) throw new InsufficientBalanceException();
        }

        /// <summary>
        /// Выбрасывает исключение, если овердрафт ниже задолженности по счету
        /// </summary>
        /// <param name="account">Счет</param>
        /// <param name="overdraft">Новая сумма овердрафта</param>
        /// <exception cref="OverdraftLessThanDebtException"></exception>
        public static void ThrowIfOverdraftIsLessThanDebt(Account account, decimal overdraft) {
            if (account.Balance.FullBalance < 0 && overdraft < -account.Balance.FullBalance) throw new OverdraftLessThanDebtException();
        }
    }
}