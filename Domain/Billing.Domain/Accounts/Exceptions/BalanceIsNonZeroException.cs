﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Accounts.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое при ненулевом балансе на счете
    /// </summary>
    public class BalanceIsNonZeroException : DomainException {
        public BalanceIsNonZeroException(string message = "Баланс не нулевой") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.BalanceIsNonZero;
    }
}