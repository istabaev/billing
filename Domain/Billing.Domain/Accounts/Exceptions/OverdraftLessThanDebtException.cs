﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Accounts.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое при установке некорректной суммы овердрафта
    /// </summary>
    public class OverdraftLessThanDebtException : DomainException {
        public OverdraftLessThanDebtException(string message = "Overdraft is less than debt") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.OverdraftIsLessThanDebt;
    }
}