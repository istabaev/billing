﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Accounts.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое при недостаточном балансе на счете
    /// </summary>
    public class InsufficientBalanceException : DomainException {
        public InsufficientBalanceException(string message = "Недостаточно средств на счете") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.InsufficientBalance;
    }
}