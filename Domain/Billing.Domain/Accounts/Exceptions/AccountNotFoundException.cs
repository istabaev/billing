﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Accounts.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое если счет не найден
    /// </summary>
    public class AccountNotFoundException : DomainException {
        public AccountNotFoundException(string message = "Счет не найден") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.AccountNotFound;
    }
}