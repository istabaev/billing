﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Accounts.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое при операциях с замороженным счетом
    /// </summary>
    public class AccountIsFrozenException : DomainException {
        public AccountIsFrozenException(string message = "Счет заморожен") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.AccountIsFrozen;
    }
}