﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Accounts.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое при операциях с закрытым счетом
    /// </summary>
    public class AccountIsClosedException : DomainException {
        public AccountIsClosedException(string message = "Счет закрыт") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.AccountIsClosed;
    }
}