﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Accounts.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое при выходе баланса за пределы лимита
    /// </summary>
    public class BalanceLimitException : DomainException {
        public BalanceLimitException(string message = "Баланс счета выходит за пределы лимита") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.BalanceIsOutOfLimit;
    }
}