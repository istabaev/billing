﻿using System;
using Billing.Domain.Amounts;

namespace Billing.Domain.Accounts {
    /// <summary>
    /// Овердрафт счета
    /// </summary>
    /// <remarks>
    /// Сумма, на которую может опуститься баланс
    /// </remarks>
    public class AccountOverdraft : Amount<AccountOverdraft> {
        private AccountOverdraft() { }

        public AccountOverdraft(decimal value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(decimal value) {
            return value >= 0;
        }
    }
}