﻿using System;
using System.Globalization;

namespace Billing.Domain.Accounts {
    /// <summary>
    /// Баланс и резерв счета
    /// </summary>
    public class AccountBalance {
        public static AccountBalance Create() => new AccountBalance(new Balance(0), new Reserve(0));

        public static AccountBalance Create(decimal balance, decimal reserve) => new AccountBalance(new Balance(balance), new Reserve(reserve));

        private AccountBalance() { }

        public AccountBalance(Balance balance, Reserve reserve) {
            Balance = balance ?? throw new ArgumentNullException(nameof(balance));
            Reserve = reserve ?? throw new ArgumentNullException(nameof(reserve));
        }

        /// <summary>
        /// Баланс
        /// </summary>
        public Balance Balance { get; private set; }

        /// <summary>
        /// Резерв
        /// </summary>
        public Reserve Reserve { get; private set; }

        /// <summary>
        /// Баланс с учетом резерва
        /// </summary>
        public decimal FullBalance => Balance.Value - Reserve.Value;

        public override bool Equals(object obj) {
            return obj is AccountBalance other &&
                other.Balance.Equals(Balance) &&
                other.Reserve.Equals(Reserve);
        }

        public override int GetHashCode() {
            return HashCode.Combine(Balance, Reserve);
        }

        public override string ToString() {
            return FullBalance.ToString(CultureInfo.InvariantCulture);
        }
    }
}