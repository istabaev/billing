﻿using System;
using Billing.Domain.Common;

namespace Billing.Domain.Amounts {
    /// <summary>
    /// Обобщенная сумма
    /// </summary>
    public class Amount<TSelf> : StructValueObject<TSelf, decimal>
        where TSelf : ValueObject<TSelf, decimal> {
        protected Amount() { }

        protected Amount(decimal value) : base(AmountHelper.Round(value)) { }
    }

    /// <summary>
    /// Сумма
    /// </summary>
    public class Amount : Amount<Amount> {
        private Amount() { }

        public Amount(decimal value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(decimal value) {
            return value != 0;
        }
    }
}