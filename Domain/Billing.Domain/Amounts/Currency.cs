﻿using Billing.Domain.Common;

namespace Billing.Domain.Amounts {
    /// <summary>
    /// Поддерживаемые валюты с трехзначными цифровыми кодами по стандарту ISO 4217
    /// </summary>
    public class Currency : Enumeration {
        /// <summary>
        /// Российский рубль
        /// </summary>
        public static readonly Currency RUB = new Currency(643, nameof(RUB));

        /// <summary>
        /// Доллар США
        /// </summary>
        public static readonly Currency USD = new Currency(840, nameof(USD));

        /// <summary>
        /// Евро
        /// </summary>
        public static readonly Currency EUR = new Currency(978, nameof(EUR));

        private Currency() { }

        private Currency(int value, string name) : base(value, name) { }
    }
}