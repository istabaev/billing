﻿using System;

namespace Billing.Domain.Amounts {
    /// <summary>
    /// Вспомогательные методы для работы с суммой
    /// </summary>
    internal static class AmountHelper {
        /// <summary>
        /// Округлить сумма до двух знаков после запятой
        /// </summary>
        /// <param name="amount">Сумма</param>
        /// <returns>Округленная сумма</returns>
        public static decimal Round(decimal amount) {
            return Math.Round(amount, 2, MidpointRounding.AwayFromZero);
        }
    }
}