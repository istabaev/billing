﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Billing.Domain.Common {
    /// <summary>
    /// Базовый класс для перечислений
    /// </summary>
    public abstract class Enumeration : IComparable {
        protected Enumeration() { }

        protected Enumeration(int value, string name) {
            Value = value;
            Name = name;
        }

        /// <summary>
        /// Значение
        /// </summary>
        public int Value { get; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Возвращает список всех перечислений по типу
        /// </summary>
        /// <typeparam name="T">Тип перечислений</typeparam>
        /// <returns></returns>
        public static IEnumerable<T> GetAll<T>() where T : Enumeration {
            FieldInfo[] fields = typeof(T).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            foreach (FieldInfo info in fields) {
                if (info.GetValue(null) is T locatedValue) {
                    yield return locatedValue;
                }
            }
        }

        /// <summary>
        /// Возвращает разницу между двумя значениями <see cref="Enumeration"/>
        /// </summary>
        /// <param name="firstValue">Первое значение <see cref="Enumeration"/></param>
        /// <param name="secondValue">Второе значение <see cref="Enumeration"/></param>
        public static int AbsoluteDifference(Enumeration firstValue, Enumeration secondValue) {
            int absoluteDifference = Math.Abs(firstValue.Value - secondValue.Value);
            return absoluteDifference;
        }

        /// <summary>
        /// Возвращает экземпляр <see cref="Enumeration"/> по значению 
        /// </summary>
        /// <param name="value">Значение</param>
        /// <typeparam name="T">Тип <see cref="Enumeration"/></typeparam>
        public static T FromValue<T>(int value) where T : Enumeration {
            T matchingItem = Parse<T, int>(value, "value", item => item.Value == value);
            return matchingItem;
        }

        /// <summary>
        /// Возвращает экземпляр <see cref="Enumeration"/> по названию
        /// </summary>
        /// <param name="name">Название</param>
        /// <typeparam name="T">Тип <see cref="Enumeration"/></typeparam>
        public static T FromName<T>(string name) where T : Enumeration {
            T matchingItem = Parse<T, string>(name, "name", item => item.Name == name);
            return matchingItem;
        }

        /// <summary>
        /// Возвращает экземпляр <see cref="Enumeration"/> по значению или названию
        /// </summary>
        /// <param name="valueOrName">Значение или название</param>
        /// <param name="enumeration">Экземпляр <see cref="Enumeration"/></param>
        /// <typeparam name="T">Тип <see cref="Enumeration"/></typeparam>
        public static bool TryParse<T>(string valueOrName, out T enumeration) where T : Enumeration {
            string name = valueOrName;
            int? value = int.TryParse(valueOrName, out int val) ? val : (int?)null;
            enumeration = GetAll<T>().FirstOrDefault(x => x.Name == name || value.HasValue && x.Value == value);
            return enumeration != null;
        }

        private static T Parse<T, TKey>(TKey value, string description, Func<T, bool> predicate) where T : Enumeration {
            T matchingItem = GetAll<T>().FirstOrDefault(predicate);

            if (matchingItem == null) {
                string message = $"'{value}' is not a valid {description} in {typeof(T)}";
                throw new ApplicationException(message);
            }

            return matchingItem;
        }

        /// <inheritdoc />
        public override string ToString() {
            return Name;
        }

        /// <inheritdoc />
        public override int GetHashCode() {
            return Value.GetHashCode();
        }

        /// <inheritdoc />
        public override bool Equals(object obj) {
            if (!(obj is Enumeration otherValue)) {
                return false;
            }

            bool typeMatches = GetType() == obj.GetType();
            bool valueMatches = Value.Equals(otherValue.Value);

            return typeMatches && valueMatches;
        }

        /// <inheritdoc />
        public int CompareTo(object other) {
            return Value.CompareTo(((Enumeration)other).Value);
        }

        public static bool operator ==(Enumeration a, Enumeration b) {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(Enumeration a, Enumeration b) {
            return !(a == b);
        }
    }
}