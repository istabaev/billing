﻿namespace Billing.Domain.Common.Exceptions {
    /// <summary>
    /// Коды ошибок
    /// </summary>
    public enum DomainErrorCode {
        /// <summary>
        /// Внутренняя ошибка
        /// </summary>
        InternalError = 0,

        /// <summary>
        /// Пользователь не найден
        /// </summary>
        UserNotFound = 100,

        /// <summary>
        /// Пользователь заблокирован
        /// </summary>
        UserIsBlocked = 110,

        /// <summary>
        /// Счет не найден
        /// </summary>
        AccountNotFound = 200,

        /// <summary>
        /// Счет заморожен
        /// </summary>
        AccountIsFrozen = 210,

        /// <summary>
        /// Счет закрыт
        /// </summary>
        AccountIsClosed = 211,

        /// <summary>
        /// Баланс счета не нулевой
        /// </summary>
        BalanceIsNonZero = 220,

        /// <summary>
        /// Баланс счета выходит за пределы лимита
        /// </summary>
        BalanceIsOutOfLimit = 221,

        /// <summary>
        /// Недостаточно средств на счете
        /// </summary>
        InsufficientBalance = 222,

        /// <summary>
        /// Некорректная сумма овердрафта
        /// </summary>
        OverdraftIsLessThanDebt = 223,

        /// <summary>
        /// Транзакция не найдена
        /// </summary>
        TransactionNotFound = 300,

        /// <summary>
        /// Некорректный статус транзакции
        /// </summary>
        IncorrectTransactionStatus = 310,

        /// <summary>
        /// Транзакция подтверждена
        /// </summary>
        TransactionIsAccepted = 311,

        /// <summary>
        /// Транзакция отменена
        /// </summary>
        TransactionIsCancelled = 312,

        /// <summary>
        /// Период не найден
        /// </summary>
        PeriodNotFound = 400,

        /// <summary>
        /// Период закрыт
        /// </summary>
        PeriodIsClosed = 410,

        /// <summary>
        /// Некорректная дата периода
        /// </summary>
        IncorrectPeriodDates = 420,

        /// <summary>
        /// Дата в закрытом периоде
        /// </summary>
        DateInClosedPeriod = 421,
    }
}