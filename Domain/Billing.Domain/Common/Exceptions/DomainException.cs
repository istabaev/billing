﻿using System;

namespace Billing.Domain.Common.Exceptions {
    /// <summary>
    /// Базовое исключение с кодом ошибки
    /// </summary>
    public abstract class DomainException : Exception {
        protected DomainException(string message) : base(message) { }

        /// <summary>
        /// Код исключения
        /// </summary>
        public abstract DomainErrorCode Code { get; }
    }
}