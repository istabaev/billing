﻿namespace Billing.Domain.Common.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое при нештатных ситуациях. Внутренняя ошибка.
    /// </summary>
    public class InternalException : DomainException {
        public InternalException(string message) : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.InternalError;
    }
}