﻿namespace Billing.Domain.Common {
    /// <summary>
    /// Сущность
    /// </summary>
    /// <remarks>Entity в DDD</remarks>
    /// <typeparam name="T">Тип идентификатора сущности</typeparam>
    public abstract class Entity<T > {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public T Id { get; protected set; }
    }
}