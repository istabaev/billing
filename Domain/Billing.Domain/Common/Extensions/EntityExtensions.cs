﻿using System.Collections.Generic;
using System.Linq;

namespace Billing.Domain.Common.Extensions {
    /// <summary>
    /// Методы расширения для сущности
    /// </summary>
    public static class EntityExtensions {
        /// <summary>
        /// Возвращает сущность из списка по его идентификатору
        /// </summary>
        /// <param name="entities">Список сущностей</param>
        /// <param name="id">Идентификатор сущности</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TId">Тип идентификатора сущности</typeparam>
        /// <returns>Найденная сущность или <code>null</code></returns>
        public static TEntity GetById<TEntity, TId>(this IEnumerable<TEntity> entities, TId id) where TEntity : Entity<TId> {
            return entities.SingleOrDefault(x => Equals(x.Id, id));
        }
    }
}