﻿using System;
using System.Globalization;

namespace Billing.Domain.Common {
    /// <summary>
    /// Объект-значение
    /// </summary>
    /// <remarks>Value Object в DDD</remarks>
    /// <typeparam name="T">Тип реализации</typeparam>
    public abstract class ValueObject<T> where T : ValueObject<T> {
        /// <summary>
        /// Проверяет другой объект на равенство
        /// </summary>
        /// <param name="other">Другой объект</param>
        protected abstract bool EqualsCore(T other);

        /// <summary>
        /// Возвращает хэш-код объекта
        /// </summary>
        protected abstract int GetHashCodeCore();

        public sealed override bool Equals(object obj) {
            return obj is T other && EqualsCore(other);
        }

        public sealed override int GetHashCode() {
            return GetHashCodeCore();
        }

        public static bool operator ==(ValueObject<T> a, ValueObject<T> b) {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(ValueObject<T> a, ValueObject<T> b) {
            return !(a == b);
        }
    }

    /// <summary>
    /// Объект-значение с обобщенным типом значения
    /// </summary>
    /// <remarks>Value Object в DDD</remarks>
    /// <typeparam name="TValueObject">Тип реализации</typeparam>
    /// <typeparam name="TValue">Тип значения</typeparam>
    public abstract class ValueObject<TValueObject, TValue> : ValueObject<TValueObject>
        where TValueObject : ValueObject<TValueObject> {

        protected ValueObject() { }

        protected ValueObject(TValue value) {
            Value = value;
        }

        /// <summary>
        /// Значение
        /// </summary>
        public TValue Value { get; private set; }
    }

    /// <summary>
    /// Объект-значение с типом структуры
    /// </summary>
    /// <typeparam name="TValueObject">Тип реализации</typeparam>
    /// <typeparam name="TValue">Тип структуры</typeparam>
    public abstract class StructValueObject<TValueObject, TValue> : ValueObject<TValueObject, TValue>
        where TValueObject : ValueObject<TValueObject, TValue>
        where TValue : struct {
        protected StructValueObject() { }

        protected StructValueObject(TValue value) : base(value) { }

        protected override bool EqualsCore(TValueObject other) {
            return Equals(Value, other.Value);
        }

        protected override int GetHashCodeCore() {
            return Value.GetHashCode();
        }

        public override string ToString() {
            return Convert.ToString(Value, CultureInfo.InvariantCulture);
        }
    }

    /// <summary>
    /// Строковое объект-значение
    /// </summary>
    /// <typeparam name="TValueObject">Тип реализации</typeparam>
    public abstract class StringValueObject<TValueObject> : ValueObject<TValueObject, string>
        where TValueObject : ValueObject<TValueObject, string> {

        protected StringValueObject() { }

        protected StringValueObject(string value) : base(value) { }

        protected override bool EqualsCore(TValueObject other) {
            return StringComparer.Ordinal.Equals(Value, other.Value);
        }

        protected override int GetHashCodeCore() {
            return StringComparer.Ordinal.GetHashCode(Value);
        }

        public override string ToString() {
            return Value;
        }
    }

    /// <summary>
    /// Объект-значение для дат
    /// </summary>
    /// <typeparam name="TValueObject">Тип реализации</typeparam>
    public abstract class DateTimeValueObject<TValueObject> : ValueObject<TValueObject, DateTime>
        where TValueObject : DateTimeValueObject<TValueObject> {
        protected DateTimeValueObject() { }

        protected DateTimeValueObject(DateTime value) : base(value) { }

        protected override bool EqualsCore(TValueObject other) {
            return Equals(Value, other.Value);
        }

        protected override int GetHashCodeCore() {
            return Value.GetHashCode();
        }
    }
}