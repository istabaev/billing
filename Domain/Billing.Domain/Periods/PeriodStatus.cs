﻿using Billing.Domain.Common;

namespace Billing.Domain.Periods {
    /// <summary>
    /// Статус периода
    /// </summary>
    public class PeriodStatus : Enumeration {
        /// <summary>
        /// Активен
        /// </summary>
        public static readonly PeriodStatus Active = new PeriodStatus(1, nameof(Active));

        /// <summary>
        /// Закрыт
        /// </summary>
        public static readonly PeriodStatus Closed = new PeriodStatus(2, nameof(Closed));

        private PeriodStatus() { }

        private PeriodStatus(int value, string name) : base(value, name) { }
    }
}