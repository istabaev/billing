﻿using System;
using System.Collections.Generic;
using System.Linq;
using Billing.Domain.Accounts;
using Billing.Domain.Accounts.Exceptions;
using Billing.Domain.Common;
using Billing.Domain.Common.Exceptions;
using Billing.Domain.DateTimes;
using Billing.Domain.Periods.Exceptions;
using Billing.Domain.Transactions;
using Billing.Domain.Users;

namespace Billing.Domain.Periods {
    /// <summary>
    /// Период
    /// </summary>
    public class Period : Entity<long> {
        private readonly List<Transaction> _transactions = new List<Transaction>();

        private Period() { }

        internal Period(User user,
                        Account account,
                        AccountBalance startBalance,
                        Date startDate,
                        Time createdTime) {
            User = user ?? throw new ArgumentNullException(nameof(user));
            Account = account ?? throw new ArgumentNullException(nameof(account));
            StartBalance = startBalance ?? throw new ArgumentNullException(nameof(startBalance));
            StartDate = startDate ?? throw new ArgumentNullException(nameof(startDate));
            CreatedTime = createdTime?.Clone() ?? throw new ArgumentNullException(nameof(createdTime));
            Status = PeriodStatus.Active;
        }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; private set; }

        /// <summary>
        /// Счет
        /// </summary>
        public Account Account { get; private set; }

        /// <summary>
        /// Статус периода
        /// </summary>
        public PeriodStatus Status { get; internal set; }

        /// <summary>
        /// Баланс и резерв счета на начало периода
        /// </summary>
        public AccountBalance StartBalance { get; private set; }

        /// <summary>
        /// Баланс и резерв счета на конец периода
        /// </summary>
        public AccountBalance EndBalance { get; internal set; }

        /// <summary>
        /// Дата начала периода (включительно)
        /// </summary>
        public Date StartDate { get; private set; }

        /// <summary>
        /// Дата конца периода (исключительно)
        /// </summary>
        public Date EndDate { get; internal set; }

        /// <summary>
        /// Дата и время создания
        /// </summary>
        public Time CreatedTime { get; private set; }

        /// <summary>
        /// Дата и время закрытия
        /// </summary>
        public Time ClosedTime { get; internal set; }

        /// <summary>
        /// Список транзакций за период
        /// </summary>
        public IReadOnlyCollection<Transaction> Transactions => _transactions.AsReadOnly();

        /// <summary>
        /// Добавляет новую транзакцию
        /// </summary>
        internal void AddTransaction(Transaction transaction) {
            _transactions.Add(transaction);
        }

        /// <summary>
        /// Добавляет список транзакций
        /// </summary>
        /// <param name="transactions">Список транзакций</param>
        private void AddTransactions(IEnumerable<Transaction> transactions) {
            _transactions.AddRange(transactions);
        }

        /// <summary>
        /// Удаляет список транзакций
        /// </summary>
        /// <param name="transactions">Список транзакций</param>
        private void RemoveTransactions(IEnumerable<Transaction> transactions) {
            foreach (Transaction transaction in transactions) {
                _transactions.Remove(transaction);
            }
        }

        /// <summary>
        /// Закрывает период
        /// </summary>
        /// <param name="now">Текущее время</param>
        /// <exception cref="PeriodIsClosedException"></exception>
        /// <exception cref="IncorrectPeriodDatesException"></exception>
        public void ClosePeriod(Time now) {
            if (now == null) throw new ArgumentNullException(nameof(now));
            PeriodValidator.ThrowIfPeriodIsClosed(this);

            Account account = Account;
            if (account.CurrentPeriod != this) throw new InternalException("Закрываемый период не является текущим периодом счета");

            Date endDate = DateTimeHelper.GetMonthBeginning(now);
            PeriodValidator.ThrowIfPeriodDatesIsIncorrect(StartDate, endDate);

            decimal beforeBalance = BalanceHelper.CalculateBalance(account);
            decimal beforeReserve = BalanceHelper.CalculateReserve(account);
            decimal balance = BalanceHelper.CalculateBalanceOnDate(account, endDate.Value);
            decimal reserve = BalanceHelper.CalculateReserveOnDate(account, endDate.Value);
            EndBalance = AccountBalance.Create(balance, reserve);
            EndDate = endDate;
            Status = PeriodStatus.Closed;
            ClosedTime = now.Clone();

            // Если счет не закрыт, создаем новый период и переносим транзакции
            if (account.Status != AccountStatus.Closed) {
                Period newPeriod = new Period(account.User, account, AccountBalance.Create(balance, reserve), endDate, now);
                IReadOnlyCollection<Transaction> oldTransactions = Transactions;
                IEnumerable<Transaction> newTransactions = oldTransactions.Where(x => (x.Date?.Value ?? x.CreatedTime.Value) >= endDate.Value);
                IEnumerable<Transaction> reservedTransactions = oldTransactions.Where(x => x.Status == TransactionStatus.Reserved);
                IEnumerable<Transaction> createdTransactions = oldTransactions.Where(x => x.Status == TransactionStatus.Created);
                IEnumerable<Transaction> transactions = newTransactions.Union(reservedTransactions).Union(createdTransactions).ToList();
                RemoveTransactions(transactions);
                newPeriod.AddTransactions(transactions);
                account.AddPeriod(newPeriod);
            }

            decimal afterBalance = BalanceHelper.CalculateBalance(account);
            if (beforeBalance != afterBalance) {
                throw new InternalException("Некорректное закрытие периода. Балансы перед и после закрытия не совпадают.");
            }

            decimal afterReserve = BalanceHelper.CalculateReserve(account);
            if (beforeReserve != afterReserve) {
                throw new InternalException("Некорректное закрытие периода. Резервы перед и после закрытия не совпадают.");
            }
        }
    }
}