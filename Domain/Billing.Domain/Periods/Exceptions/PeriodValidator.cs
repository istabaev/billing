﻿using Billing.Domain.Accounts;
using Billing.Domain.DateTimes;

namespace Billing.Domain.Periods.Exceptions {
    /// <summary>
    /// Методы валидации периода
    /// </summary>
    internal static class PeriodValidator {
        /// <summary>
        /// Выбрасывает исключение, если период закрыт
        /// </summary>
        /// <param name="period">Период</param>
        /// <exception cref="PeriodIsClosedException"></exception>
        public static void ThrowIfPeriodIsClosed(Period period) {
            if (period.Status == PeriodStatus.Closed) throw new PeriodIsClosedException();
        }

        /// <summary>
        /// Выбрасывает исключение при некорректных датах периода
        /// </summary>
        /// <param name="startDate">Дата начала периода</param>
        /// <param name="endDate">Дата окончания периода</param>
        /// <exception cref="IncorrectPeriodDatesException"></exception>
        public static void ThrowIfPeriodDatesIsIncorrect(Date startDate, Date endDate) {
            if (startDate.Value >= endDate.Value) throw new IncorrectPeriodDatesException();
            if (startDate.Value.Month >= endDate.Value.Month) throw new IncorrectPeriodDatesException();
        }

        /// <summary>
        /// Выбрасывает исключение, если дата находится в закрытом периоде
        /// </summary>
        /// <param name="account">Счет</param>
        /// <param name="date">Дата</param>
        /// <exception cref="DateInClosedPeriodException"></exception>
        public static void ThrowIfDateInClosedPeriod(Account account, Date date) {
            Period currentPeriod = account.CurrentPeriod;
            if (date < currentPeriod.StartDate) throw new DateInClosedPeriodException();
            if (date < currentPeriod.EndDate && currentPeriod.Status == PeriodStatus.Closed) throw new DateInClosedPeriodException();
        }
    }
}