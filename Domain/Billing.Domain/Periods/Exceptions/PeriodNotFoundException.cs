﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Periods.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое если период не найден
    /// </summary>
    public class PeriodNotFoundException : DomainException {
        public PeriodNotFoundException(string message = "Период не найден") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.PeriodNotFound;
    }
}