﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Periods.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое если за указанную дату период закрыт
    /// </summary>
    public class DateInClosedPeriodException : DomainException {
        public DateInClosedPeriodException(string message = "Период за указанную дату закрыт") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.DateInClosedPeriod;
    }
}