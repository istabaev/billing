﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Periods.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое если период закрыт
    /// </summary>
    public class PeriodIsClosedException : DomainException {
        public PeriodIsClosedException(string message = "Период закрыт") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.PeriodIsClosed;
    }
}