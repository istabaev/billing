﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Periods.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое при некорректных датах периода
    /// </summary>
    public class IncorrectPeriodDatesException : DomainException {
        public IncorrectPeriodDatesException(string message = "Некорректная дата периода") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.IncorrectPeriodDates;
    }
}