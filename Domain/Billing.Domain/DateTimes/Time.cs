﻿using System;
using Billing.Domain.Common;

namespace Billing.Domain.DateTimes {
    /// <summary>
    /// Дата и время в UTC
    /// </summary>
    public class Time : DateTimeValueObject<Time>, IComparable, IComparable<Time>, IComparable<Date>, IComparable<DateTime> {
        private Time() { }

        public Time(DateTime value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(DateTime value) {
            return value != new DateTime() && value.Kind == DateTimeKind.Utc;
        }

        public Time Clone() {
            return new Time(Value);
        }

        public override string ToString() {
            return Value.ToString("yyyy-MM-dd hh:mm:ss.fff");
        }

        #region IComparable implementation

        public int CompareTo(object obj) {
            int compare = obj switch {
                null => 1,
                Time time => CompareTo(time),
                Date date => CompareTo(date),
                DateTime dateTime => CompareTo(dateTime),
                _ => throw new ArgumentException("Invalid type for comparison")
            };
            return compare;
        }

        public int CompareTo(Time other) {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(Date other) {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(DateTime other) {
            return Value.CompareTo(other);
        }

        #endregion

        #region Time-Time comparison operators

        public static bool operator <(Time v1, Time v2) {
            return v1.CompareTo(v2) < 0;
        }

        public static bool operator >(Time v1, Time v2) {
            return v1.CompareTo(v2) > 0;
        }

        public static bool operator <=(Time v1, Time v2) {
            return v1.CompareTo(v2) <= 0;
        }

        public static bool operator >=(Time v1, Time v2) {
            return v1.CompareTo(v2) >= 0;
        }

        #endregion

        #region Time-Date comparison operators

        public static bool operator <(Time v1, Date v2) {
            return v1.CompareTo(v2) < 0;
        }

        public static bool operator >(Time v1, Date v2) {
            return v1.CompareTo(v2) > 0;
        }

        public static bool operator <=(Time v1, Date v2) {
            return v1.CompareTo(v2) <= 0;
        }

        public static bool operator >=(Time v1, Date v2) {
            return v1.CompareTo(v2) >= 0;
        }

        #endregion

        #region Time-DateTime comparison operators

        public static bool operator <(Time v1, DateTime v2) {
            return v1.CompareTo(v2) < 0;
        }

        public static bool operator >(Time v1, DateTime v2) {
            return v1.CompareTo(v2) > 0;
        }

        public static bool operator <=(Time v1, DateTime v2) {
            return v1.CompareTo(v2) <= 0;
        }

        public static bool operator >=(Time v1, DateTime v2) {
            return v1.CompareTo(v2) >= 0;
        }

        #endregion
    }
}