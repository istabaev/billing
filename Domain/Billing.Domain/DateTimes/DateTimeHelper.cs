﻿using System;

namespace Billing.Domain.DateTimes {
    /// <summary>
    /// Вспомогательные методы для работы с датой и временем
    /// </summary>
    internal static class DateTimeHelper {
        /// <summary>
        /// Возвращает начало месяца для указанной даты
        /// </summary>
        /// <param name="date">Дата</param>
        /// <returns></returns>
        public static Date GetMonthBeginning(DateTime date) {
            DateTime monthBeginning = date.Subtract(TimeSpan.FromDays(date.Day - 1)).Date;
            return new Date(monthBeginning);
        }

        /// <summary>
        /// Возвращает начало месяца для указанной даты
        /// </summary>
        /// <param name="date">Дата</param>
        /// <returns></returns>
        public static Date GetMonthBeginning(Date date) {
            return GetMonthBeginning(date.Value);
        }

        /// <summary>
        /// Возвращает начало месяца для указанной даты
        /// </summary>
        /// <param name="time">Дата</param>
        /// <returns></returns>
        public static Date GetMonthBeginning(Time time) {
            return GetMonthBeginning(time.Value);
        }
    }
}