﻿using System;
using Billing.Domain.Common;

namespace Billing.Domain.DateTimes {
    /// <summary>
    /// Дата без времени
    /// </summary>
    public class Date : DateTimeValueObject<Date>, IComparable, IComparable<Date>, IComparable<Time>, IComparable<DateTime> {
        private Date() { }

        public Date(DateTime value) : base(value.Date) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(DateTime value) {
            return value != new DateTime();
        }

        public Date Clone() {
            return new Date(Value);
        }

        public override string ToString() {
            return Value.ToString("yyyy-MM-dd");
        }

        #region IComparable implementation

        public int CompareTo(object obj) {
            int compare = obj switch {
                null => 1,
                Date date => CompareTo(date),
                Time time => CompareTo(time),
                DateTime dateTime => CompareTo(dateTime),
                _ => throw new ArgumentException("Invalid type for comparison")
            };
            return compare;
        }

        public int CompareTo(Date other) {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(Time other) {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(DateTime other) {
            return Value.CompareTo(other);
        }

        #endregion

        #region Date-Date comparison operators

        public static bool operator <(Date v1, Date v2) {
            return v1.CompareTo(v2) < 0;
        }

        public static bool operator >(Date v1, Date v2) {
            return v1.CompareTo(v2) > 0;
        }

        public static bool operator <=(Date v1, Date v2) {
            return v1.CompareTo(v2) <= 0;
        }

        public static bool operator >=(Date v1, Date v2) {
            return v1.CompareTo(v2) >= 0;
        }

        #endregion

        #region Date-Time comparison operators

        public static bool operator <(Date v1, Time v2) {
            return v1.CompareTo(v2) < 0;
        }

        public static bool operator >(Date v1, Time v2) {
            return v1.CompareTo(v2) > 0;
        }

        public static bool operator <=(Date v1, Time v2) {
            return v1.CompareTo(v2) <= 0;
        }

        public static bool operator >=(Date v1, Time v2) {
            return v1.CompareTo(v2) >= 0;
        }

        #endregion

        #region Date-DateTime comparison operators

        public static bool operator <(Date v1, DateTime v2) {
            return v1.CompareTo(v2) < 0;
        }

        public static bool operator >(Date v1, DateTime v2) {
            return v1.CompareTo(v2) > 0;
        }

        public static bool operator <=(Date v1, DateTime v2) {
            return v1.CompareTo(v2) <= 0;
        }

        public static bool operator >=(Date v1, DateTime v2) {
            return v1.CompareTo(v2) >= 0;
        }

        #endregion
    }
}