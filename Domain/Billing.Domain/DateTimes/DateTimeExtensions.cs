﻿using System;

namespace Billing.Domain.DateTimes {
    /// <summary>
    /// Методы расширения для <see cref="DateTime"/>
    /// </summary>
    public static class DateTimeExtensions {
        /// <summary>
        /// Возвращает дату <see cref="Date"/> для указанного <see cref="DateTime"/>
        /// </summary>
        /// <param name="dateTime">Дата и время</param>
        public static Date ToDate(this DateTime dateTime) {
            return new Date(dateTime);
        }

        /// <summary>
        /// Возвращает дату и время <see cref="Time"/> для указанного <see cref="DateTime"/>
        /// </summary>
        /// <param name="dateTime">Дата и время</param>
        public static Time ToTime(this DateTime dateTime) {
            return new Time(dateTime);
        }
    }
}