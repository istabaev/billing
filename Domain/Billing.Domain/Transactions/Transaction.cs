﻿using System;
using Billing.Domain.Accounts;
using Billing.Domain.Accounts.Exceptions;
using Billing.Domain.Amounts;
using Billing.Domain.Common;
using Billing.Domain.DateTimes;
using Billing.Domain.Periods;
using Billing.Domain.Periods.Exceptions;
using Billing.Domain.Transactions.Exceptions;
using Billing.Domain.Users;
using Billing.Domain.Users.Exceptions;

namespace Billing.Domain.Transactions {
    /// <summary>
    /// Транзакция по счету
    /// </summary>
    public class Transaction : Entity<long> {
        private Transaction() { }

        internal Transaction(User user,
                             Account account,
                             Period period,
                             Amount amount,
                             Currency currency,
                             TransactionType type,
                             TransactionDescription description,
                             Time createdTime) {
            User = user ?? throw new ArgumentNullException(nameof(user));
            Account = account ?? throw new ArgumentNullException(nameof(account));

            UserValidator.ThrowIfUserIsBlocked(user);
            AccountValidator.ThrowIfAccountIsFrozen(account);
            AccountValidator.ThrowIfAccountIsClosed(account);

            Period = period ?? throw new ArgumentNullException(nameof(period));
            Currency = currency ?? throw new ArgumentNullException(nameof(currency));
            Status = TransactionStatus.Created;
            Amount = amount ?? throw new ArgumentNullException(nameof(amount));
            Type = type ?? throw new ArgumentNullException(nameof(type));
            Description = description ?? throw new ArgumentNullException(nameof(description));
            CreatedTime = createdTime?.Clone() ?? throw new ArgumentNullException(nameof(createdTime));
        }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; private set; }

        /// <summary>
        /// Счет
        /// </summary>
        public Account Account { get; private set; }

        /// <summary>
        /// Период
        /// </summary>
        public Period Period { get; private set; }

        /// <summary>
        /// Статус
        /// </summary>
        public TransactionStatus Status { get; internal set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public Amount Amount { get; private set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public Currency Currency { get; private set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public TransactionType Type { get; private set; }

        /// <summary>
        /// Описание
        /// </summary>
        public TransactionDescription Description { get; set; }

        /// <summary>
        /// Дата проведения
        /// </summary>
        public Date Date { get; internal set; }

        /// <summary>
        /// Дата и время создания
        /// </summary>
        public Time CreatedTime { get; private set; }

        /// <summary>
        /// Дата и время резервирования
        /// </summary>
        public Time ReservedTime { get; internal set; }

        /// <summary>
        /// Дата и время отмены
        /// </summary>
        public Time CancelledTime { get; internal set; }

        /// <summary>
        /// Дата и время подтверждения
        /// </summary>
        public Time AcceptedTime { get; internal set; }

        /// <summary>
        /// Резервирует средства со счета на сумму транзакции
        /// </summary>
        /// <param name="now">Текущее время</param>
        /// <returns><code>true</code> - если статус изменился</returns>
        /// <exception cref="UserIsBlockedException"></exception>
        /// <exception cref="AccountIsFrozenException"></exception>
        /// <exception cref="AccountIsClosedException"></exception>
        /// <exception cref="TransactionIsAcceptedException"></exception>
        /// <exception cref="TransactionIsCancelledException"></exception>
        /// <exception cref="IncorrectTransactionStatusException"></exception>
        /// <exception cref="BalanceLimitException"></exception>
        /// <exception cref="InsufficientBalanceException"></exception>
        public bool Reserve(Time now) {
            if (now == null) throw new ArgumentNullException(nameof(now));

            Account account = Account;
            UserValidator.ThrowIfUserIsBlocked(User);
            AccountValidator.ThrowIfAccountIsFrozen(account);
            AccountValidator.ThrowIfAccountIsClosed(account);
            TransactionValidator.ThrowIfTransactionStatusIsIncorrect(this, TransactionStatus.Reserved);
            AccountValidator.ThrowIfBalanceIsOutOfLimit(account, Amount.Value);

            if (Status == TransactionStatus.Reserved) {
                return false;
            }

            Status = TransactionStatus.Reserved;
            ReservedTime = now.Clone();
            account.Balance = BalanceHelper.CalculateFullBalance(account);
            return true;
        }

        /// <summary>
        /// Подтверждает транзакцию
        /// </summary>
        /// <param name="now">Текущее время</param>
        /// <param name="date">Дата проведения</param>
        /// <returns><code>true</code> - если статус изменился</returns>
        /// <exception cref="UserIsBlockedException"></exception>
        /// <exception cref="AccountIsClosedException"></exception>
        /// <exception cref="TransactionIsAcceptedException"></exception>
        /// <exception cref="TransactionIsCancelledException"></exception>
        /// <exception cref="IncorrectTransactionStatusException"></exception>
        /// <exception cref="BalanceLimitException"></exception>
        /// <exception cref="InsufficientBalanceException"></exception>
        /// <exception cref="PeriodIsClosedException"></exception>
        /// <exception cref="DateInClosedPeriodException"></exception>
        public bool Accept(Time now, Date date = null) {
            if (now == null) throw new ArgumentNullException(nameof(now));

            Account account = Account;
            UserValidator.ThrowIfUserIsBlocked(User);
            AccountValidator.ThrowIfAccountIsClosed(account);
            TransactionValidator.ThrowIfTransactionStatusIsIncorrect(this, TransactionStatus.Accepted);
            if (Status != TransactionStatus.Reserved) {
                AccountValidator.ThrowIfBalanceIsOutOfLimit(account, Amount.Value);
            }
            PeriodValidator.ThrowIfPeriodIsClosed(Period);
            if (date != null) {
                PeriodValidator.ThrowIfDateInClosedPeriod(account, date);
            }

            if (Status == TransactionStatus.Accepted) {
                return false;
            }

            Status = TransactionStatus.Accepted;
            AcceptedTime = now.Clone();
            if (date != null) {
                Date = date;
            } else if (Date == null) {
                Date = now.Value.ToDate();
            }
            account.Balance = BalanceHelper.CalculateFullBalance(account);
            return true;
        }

        /// <summary>
        /// Отменяет транзакцию
        /// </summary>
        /// <param name="now">Текущее время</param>
        /// <returns><code>true</code> - если статус изменился</returns>
        /// <exception cref="AccountIsFrozenException"></exception>
        /// <exception cref="AccountIsClosedException"></exception>
        /// <exception cref="TransactionIsAcceptedException"></exception>
        /// <exception cref="TransactionIsCancelledException"></exception>
        /// <exception cref="IncorrectTransactionStatusException"></exception>
        public bool Cancel(Time now) {
            if (now == null) throw new ArgumentNullException(nameof(now));

            Account account = Account;
            AccountValidator.ThrowIfAccountIsFrozen(account);
            AccountValidator.ThrowIfAccountIsClosed(account);
            TransactionValidator.ThrowIfTransactionStatusIsIncorrect(this, TransactionStatus.Cancelled);

            if (Status == TransactionStatus.Cancelled) {
                return false;
            }

            Status = TransactionStatus.Cancelled;
            CancelledTime = now.Clone();
            account.Balance = BalanceHelper.CalculateFullBalance(account);
            return true;
        }
    }
}