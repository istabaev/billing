﻿using System;
using Billing.Domain.Common;

namespace Billing.Domain.Transactions {
    public class TransactionType : StringValueObject<TransactionType> {
        public const int MAX_LENGTH = 32;

        private TransactionType() { }

        public TransactionType(string value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(string value) {
            return !string.IsNullOrWhiteSpace(value) && value.Length <= MAX_LENGTH;
        }
    }
}