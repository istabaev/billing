﻿using System;
using Billing.Domain.Common;

namespace Billing.Domain.Transactions {
    public class TransactionDescription : StringValueObject<TransactionDescription> {
        public const int MAX_LENGTH = 256;

        private TransactionDescription() { }

        public TransactionDescription(string value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(string value) {
            return !string.IsNullOrWhiteSpace(value) && value.Length <= MAX_LENGTH;
        }
    }
}