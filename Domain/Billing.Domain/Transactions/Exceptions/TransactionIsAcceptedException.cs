﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Transactions.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое если транзакция подтверждена
    /// </summary>
    public class TransactionIsAcceptedException : DomainException {
        public TransactionIsAcceptedException(string message = "Транзакция подтверждена") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.TransactionIsAccepted;
    }
}