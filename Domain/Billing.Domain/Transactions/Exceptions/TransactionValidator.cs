﻿using System;
using System.Collections.Generic;
using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Transactions.Exceptions {
    /// <summary>
    /// Методы валидации транзакции
    /// </summary>
    internal static class TransactionValidator {
        /// <summary>
        /// Карта перехода статусов транзакции
        /// </summary>
        private static readonly Dictionary<TransactionStatus, HashSet<TransactionStatus>> _statusMap =
            new Dictionary<TransactionStatus, HashSet<TransactionStatus>> {
                { TransactionStatus.Created, new HashSet<TransactionStatus> { TransactionStatus.Reserved, TransactionStatus.Accepted, TransactionStatus.Cancelled } },
                { TransactionStatus.Reserved, new HashSet<TransactionStatus> { TransactionStatus.Accepted, TransactionStatus.Cancelled } }
            };

        /// <summary>
        /// Исключения финальных статусов транзакции
        /// </summary>
        private static readonly Dictionary<TransactionStatus, Func<DomainException>> _finalStatusExceptions =
            new Dictionary<TransactionStatus, Func<DomainException>> {
                { TransactionStatus.Accepted, () => new TransactionIsAcceptedException() },
                { TransactionStatus.Cancelled, () => new TransactionIsCancelledException() }
            };

        /// <summary>
        /// Финальные статусы транзакции
        /// </summary>
        private static readonly HashSet<TransactionStatus> _finalStatuses =
            new HashSet<TransactionStatus>(_finalStatusExceptions.Keys);

        /// <summary>
        /// Выбрасывает исключение, если новый статус транзакции неверный
        /// </summary>
        /// <param name="transaction">Транзакция</param>
        /// <param name="status">Новый статус транзакции</param>
        /// <exception cref="TransactionIsAcceptedException"></exception>
        /// <exception cref="TransactionIsCancelledException"></exception>
        /// <exception cref="IncorrectTransactionStatusException"></exception>
        public static void ThrowIfTransactionStatusIsIncorrect(Transaction transaction, TransactionStatus status) {
            ThrowIfTransactionStatusIsFinal(transaction);
            if (!_statusMap[transaction.Status].Contains(status)) throw new IncorrectTransactionStatusException();
            if (transaction.Amount.Value > 0 && status == TransactionStatus.Reserved) throw new IncorrectTransactionStatusException();
        }

        /// <summary>
        /// Выбрасывает исключение, если транзакция в конечном статусе
        /// </summary>
        /// <param name="transaction">Транзакция</param>
        /// <exception cref="TransactionIsAcceptedException"></exception>
        /// <exception cref="TransactionIsCancelledException"></exception>
        public static void ThrowIfTransactionStatusIsFinal(Transaction transaction) {
            if (_finalStatuses.Contains(transaction.Status)) throw _finalStatusExceptions[transaction.Status]();
        }

        /// <summary>
        /// Выбрасывает исключение, если транзакция отменена
        /// </summary>
        /// <param name="transaction">Транзакция</param>
        /// <exception cref="TransactionIsCancelledException"></exception>
        public static void ThrowIfTransactionStatusIsCancelled(Transaction transaction) {
            if (transaction.Status == TransactionStatus.Cancelled) throw _finalStatusExceptions[transaction.Status]();
        }
    }
}