﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Transactions.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое при некорректном переходе статусов
    /// </summary>
    public class IncorrectTransactionStatusException : DomainException {
        public IncorrectTransactionStatusException(string message = "Некорректный статус транзакции") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.IncorrectTransactionStatus;
    }
}