﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Transactions.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое если транзакция не найдена
    /// </summary>
    public class TransactionNotFoundException : DomainException {
        public TransactionNotFoundException(string message = "Транзакция не найдена") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.TransactionNotFound;
    }
}