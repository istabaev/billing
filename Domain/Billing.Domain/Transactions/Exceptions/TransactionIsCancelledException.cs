﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Transactions.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое если транзакция отменена
    /// </summary>
    public class TransactionIsCancelledException : DomainException {
        public TransactionIsCancelledException(string message = "Транзакция отменена") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.TransactionIsCancelled;
    }
}