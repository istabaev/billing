﻿using Billing.Domain.Common;

namespace Billing.Domain.Transactions {
    /// <summary>
    /// Статус транзакции
    /// </summary>
    public class TransactionStatus : Enumeration {
        /// <summary>
        /// Создана
        /// </summary>
        public static readonly TransactionStatus Created = new TransactionStatus(0, nameof(Created));

        /// <summary>
        /// Зарезервирована
        /// </summary>
        public static readonly TransactionStatus Reserved = new TransactionStatus(1, nameof(Reserved));

        /// <summary>
        /// Подтверждена
        /// </summary>
        public static readonly TransactionStatus Accepted = new TransactionStatus(2, nameof(Accepted));

        /// <summary>
        /// Отменена
        /// </summary>
        public static readonly TransactionStatus Cancelled = new TransactionStatus(3, nameof(Cancelled));

        private TransactionStatus() { }

        private TransactionStatus(int value, string name) : base(value, name) { }
    }
}