﻿using Billing.Domain.Amounts;
using Billing.Domain.DateTimes;

namespace Billing.Domain.Transactions.Contracts {
    /// <summary>
    /// Параметры, необходимые при создании транзакции
    /// </summary>
    public class CreateTransactionParams {
        /// <summary>
        /// Сумма
        /// </summary>
        public Amount Amount { get; set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public TransactionType Type { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public TransactionDescription Description { get; set; }

        /// <summary>
        /// Текущее время в UTC
        /// </summary>
        public Time Now { get; set; }
    }
}