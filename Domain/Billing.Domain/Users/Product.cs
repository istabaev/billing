﻿using System;
using Billing.Domain.Common;

namespace Billing.Domain.Users {
    /// <summary>
    /// Продукт, в котором зарегистрирован пользователь
    /// </summary>
    public class Product : StringValueObject<Product> {
        public const int MAX_LENGTH = 32;

        private Product() { }

        public Product(string value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(string value) {
            return !string.IsNullOrWhiteSpace(value) && value.Length <= MAX_LENGTH;
        }
    }
}