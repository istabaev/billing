﻿using Billing.Domain.Common;

namespace Billing.Domain.Users {
    /// <summary>
    /// Статус пользователя
    /// </summary>
    public class UserStatus : Enumeration {
        /// <summary>
        /// Активен
        /// </summary>
        public static readonly UserStatus Active = new UserStatus(1, nameof(Active));

        /// <summary>
        /// Заблокирован
        /// </summary>
        public static readonly UserStatus Blocked = new UserStatus(2, nameof(Blocked));

        private UserStatus() { }

        private UserStatus(int value, string name) : base(value, name) { }
    }
}