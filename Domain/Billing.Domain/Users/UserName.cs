﻿using System;
using Billing.Domain.Common;

namespace Billing.Domain.Users {
    /// <summary>
    /// Имя пользователя
    /// </summary>
    public class UserName : StringValueObject<UserName> {
        public const int MAX_LENGTH = 256;

        private UserName() { }

        public UserName(string value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(string value) {
            return !string.IsNullOrWhiteSpace(value) && value.Length <= MAX_LENGTH;
        }
    }
}