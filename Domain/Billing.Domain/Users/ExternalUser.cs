﻿using System;
using Billing.Domain.Common;

namespace Billing.Domain.Users {
    /// <summary>
    /// Внешний пользователь биллинга
    /// </summary>
    public class ExternalUser : ValueObject<ExternalUser> {
        private ExternalUser() { }

        public ExternalUser(Product product, ExternalId externalId) {
            Product = product ?? throw new ArgumentNullException(nameof(product));
            ExternalId = externalId ?? throw new ArgumentNullException(nameof(externalId));
        }

        public Product Product { get; private set; }

        public ExternalId ExternalId { get; private set; }

        protected override bool EqualsCore(ExternalUser other) {
            return Equals(Product, other.Product) &&
                   Equals(ExternalId, other.ExternalId);
        }

        protected override int GetHashCodeCore() {
            return HashCode.Combine(Product, ExternalId);
        }

        public override String ToString() {
            return $"{Product.Value}-{ExternalId.Value}";
        }
    }
}