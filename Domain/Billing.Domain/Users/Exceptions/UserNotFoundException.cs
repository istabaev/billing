﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Users.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое если пользователь не найден
    /// </summary>
    public class UserNotFoundException : DomainException {
        public UserNotFoundException(string message = "Пользователь не найден") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.UserNotFound;
    }
}