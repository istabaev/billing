﻿namespace Billing.Domain.Users.Exceptions {
    /// <summary>
    /// Методы валидации пользователя
    /// </summary>
    internal static class UserValidator {
        /// <summary>
        /// Выбрасывает исключение, если пользователь заблокирован
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <exception cref="UserIsBlockedException"></exception>
        public static void ThrowIfUserIsBlocked(User user) {
            if (user.Status == UserStatus.Blocked) throw new UserIsBlockedException();
        }
    }
}