﻿using Billing.Domain.Common.Exceptions;

namespace Billing.Domain.Users.Exceptions {
    /// <summary>
    /// Исключение, выбрасываемое при операциях с заблокированным пользователем
    /// </summary>
    public class UserIsBlockedException : DomainException {
        public UserIsBlockedException(string message = "Пользователь заблокирован") : base(message) { }

        public override DomainErrorCode Code => DomainErrorCode.UserIsBlocked;
    }
}