﻿using System;
using Billing.Domain.Common;

namespace Billing.Domain.Users {
    /// <summary>
    /// Внешний идентификатор пользователя
    /// </summary>
    public class ExternalId : StringValueObject<ExternalId> {
        public const int MAX_LENGTH = 64;

        public ExternalId(string value) : base(value) {
            if (!IsValid(value)) throw new ArgumentException($"{GetType().Name} is not valid", nameof(value));
        }

        public static bool IsValid(string value) {
            return !string.IsNullOrWhiteSpace(value) && value.Length <= MAX_LENGTH;
        }
    }
}