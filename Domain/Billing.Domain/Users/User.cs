﻿using System;
using System.Collections.Generic;
using Billing.Domain.Accounts;
using Billing.Domain.Accounts.Contracts;
using Billing.Domain.Common;
using Billing.Domain.DateTimes;
using Billing.Domain.Periods;

namespace Billing.Domain.Users {
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User : Entity<long> {
        private readonly List<Account> _accounts = new List<Account>();

        private User() { }

        public User(ExternalUser externalUser, UserName name, Time createdTime) {
            ExternalUser = externalUser ?? throw new ArgumentNullException(nameof(externalUser));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Status = UserStatus.Active;
            CreatedTime = createdTime?.Clone() ?? throw new ArgumentNullException(nameof(createdTime));
            ActivatedTime = createdTime.Clone();
        }

        /// <summary>
        /// Внешний пользователь
        /// </summary>
        public ExternalUser ExternalUser { get; private set; }

        /// <summary>
        /// Имя
        /// </summary>
        public UserName Name { get; private set; }

        /// <summary>
        /// Статус
        /// </summary>
        public UserStatus Status { get; internal set; }

        /// <summary>
        /// Дата и время регистрации
        /// </summary>
        public Time CreatedTime { get; private set; }

        /// <summary>
        /// Дата и время активации
        /// </summary>
        public Time ActivatedTime { get; internal set; }

        /// <summary>
        /// Дата и время блокировки
        /// </summary>
        public Time BlockedTime { get; internal set; }

        /// <summary>
        /// Список счетов
        /// </summary>
        public IReadOnlyCollection<Account> Accounts => _accounts.AsReadOnly();

        /// <summary>
        /// Активирует пользователя
        /// </summary>
        /// <param name="now">Текущее время в UTC</param>
        /// <returns><code>true</code> - если статус изменился</returns>
        public bool Activate(Time now) {
            if (now == null) throw new ArgumentNullException(nameof(now));

            if (Status == UserStatus.Active) {
                return false;
            }

            Status = UserStatus.Active;
            ActivatedTime = now.Clone();
            return true;
        }

        /// <summary>
        /// Блокирует пользователя
        /// </summary>
        /// <param name="now">Текущее время в UTC</param>
        /// <returns><code>true</code> - если статус изменился</returns>
        public bool Block(Time now) {
            if (now == null) throw new ArgumentNullException(nameof(now));

            if (Status == UserStatus.Blocked) {
                return false;
            }

            Status = UserStatus.Blocked;
            BlockedTime = now.Clone();
            return true;
        }

        /// <summary>
        /// Добавляет новый счет пользователю
        /// </summary>
        /// <param name="params">Параметры для создания нового счета</param>
        public Account AddAccount(CreateAccountParams @params) {
            if (@params == null) throw new ArgumentNullException(nameof(@params));
            Account account = new Account(this, @params.Currency, @params.Description, @params.Now);
            Date startDate = DateTimeHelper.GetMonthBeginning(@params.Now);
            Period period = new Period(this, account, AccountBalance.Create(), startDate, @params.Now);
            account.AddPeriod(period);
            _accounts.Add(account);
            return account;
        }
    }
}