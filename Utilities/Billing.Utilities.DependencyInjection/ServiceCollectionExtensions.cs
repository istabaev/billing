﻿using Billing.Utilities.Providers;
using Billing.Utilities.Providers.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace Billing.Utilities.DependencyInjection {
    /// <summary>
    /// Методы расширения для регистрации сервисов <see cref="Billing.Utilities"/> в <see cref="IServiceCollection" />
    /// </summary>
    public static class ServiceCollectionExtensions {
        /// <summary>
        /// Регистрирует сервисы <see cref="Billing.Utilities"/>
        /// </summary>
        /// <param name="services">Коллекция <see cref="IServiceCollection" />, в которую добавляются сервисы</param>
        public static void AddUtilities(this IServiceCollection services) {
            services.AddSingleton<IDateTimeProvider, DateTimeProvider>();
        }
    }
}