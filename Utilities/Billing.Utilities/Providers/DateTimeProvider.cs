﻿using System;
using Billing.Utilities.Providers.Contracts;

namespace Billing.Utilities.Providers {
    /// <summary>
    /// Предоставляет доступ к текущей дате и времени
    /// </summary>
    public class DateTimeProvider : IDateTimeProvider {
        /// <inheritdoc />
        public DateTime Now => DateTime.UtcNow;
    }
}