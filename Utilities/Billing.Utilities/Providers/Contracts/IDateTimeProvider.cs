﻿using System;

namespace Billing.Utilities.Providers.Contracts {
    /// <summary>
    /// Предоставляет доступ к текущей дате и времени
    /// </summary>
    public interface IDateTimeProvider {
        /// <summary>
        /// Текущее время в UTC
        /// </summary>
        DateTime Now { get; }
    }
}