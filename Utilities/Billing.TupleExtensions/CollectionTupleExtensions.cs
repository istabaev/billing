﻿using System;
using System.Collections.Generic;

namespace Billing.TupleExtensions {
    public static class CollectionTupleExtensions {
        public static IEnumerable<T> ToEnumerable<T>(this ValueTuple<T> values) {
            yield return values.Item1;
        }

        public static IEnumerable<T> ToEnumerable<T>(this Tuple<T> values) {
            yield return values.Item1;
        }

        public static IEnumerable<T> ToEnumerable<T>(this (T, T) values) {
            yield return values.Item1;
            yield return values.Item2;
        }

        public static IEnumerable<T> ToEnumerable<T>(this (T, T, T) values) {
            yield return values.Item1;
            yield return values.Item2;
            yield return values.Item3;
        }

        public static IEnumerable<T> ToEnumerable<T>(this (T, T, T, T) values) {
            yield return values.Item1;
            yield return values.Item2;
            yield return values.Item3;
            yield return values.Item4;
        }

        public static IEnumerable<T> ToEnumerable<T>(this (T, T, T, T, T) values) {
            yield return values.Item1;
            yield return values.Item2;
            yield return values.Item3;
            yield return values.Item4;
            yield return values.Item5;
        }

        public static IEnumerable<T> ToEnumerable<T>(this (T, T, T, T, T, T) values) {
            yield return values.Item1;
            yield return values.Item2;
            yield return values.Item3;
            yield return values.Item4;
            yield return values.Item5;
            yield return values.Item6;
        }

        public static IEnumerable<T> ToEnumerable<T>(this (T, T, T, T, T, T, T) values) {
            yield return values.Item1;
            yield return values.Item2;
            yield return values.Item3;
            yield return values.Item4;
            yield return values.Item5;
            yield return values.Item6;
            yield return values.Item7;
        }

        public static IEnumerable<T> ToEnumerable<T>(this (T, T, T, T, T, T, T, T) values) {
            yield return values.Item1;
            yield return values.Item2;
            yield return values.Item3;
            yield return values.Item4;
            yield return values.Item5;
            yield return values.Item6;
            yield return values.Item7;
            yield return values.Item8;
        }

        public static IEnumerable<T> ToEnumerable<T>(this (T, T, T, T, T, T, T, T, T) values) {
            yield return values.Item1;
            yield return values.Item2;
            yield return values.Item3;
            yield return values.Item4;
            yield return values.Item5;
            yield return values.Item6;
            yield return values.Item7;
            yield return values.Item8;
            yield return values.Item9;
        }

        public static IEnumerable<T> ToEnumerable<T>(this (T, T, T, T, T, T, T, T, T, T) values) {
            yield return values.Item1;
            yield return values.Item2;
            yield return values.Item3;
            yield return values.Item4;
            yield return values.Item5;
            yield return values.Item6;
            yield return values.Item7;
            yield return values.Item8;
            yield return values.Item9;
            yield return values.Item10;
        }
    }
}