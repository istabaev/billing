﻿namespace CQRS.Commands {
    /// <summary>
    /// Предоставляет механизм для выполнения команд
    /// </summary>
    /// <typeparam name="TCommand">Тип команды</typeparam>
    public interface ICommandHandler<in TCommand> where TCommand : ICommand {
        /// <summary>
        /// Выполняет переданную команду
        /// </summary>
        /// <param name="command">Команда</param>
        void Handle(TCommand command);
    }

    /// <summary>
    /// Предоставляет механизм для выполнения команд и возвращения результата
    /// </summary>
    /// <typeparam name="TCommand">Тип команды</typeparam>
    /// <typeparam name="TResult">Тип результата</typeparam>
    public interface ICommandHandler<in TCommand, out TResult> where TCommand : ICommand {
        /// <summary>
        /// Выполняет переданную команду
        /// </summary>
        /// <param name="command">Команда</param>
        /// <returns>Результат выполнения команды</returns>
        TResult Handle(TCommand command);
    }
}