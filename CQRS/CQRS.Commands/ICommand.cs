﻿namespace CQRS.Commands {
    /// <summary>
    /// Базовый интерфейс для всех команд
    /// </summary>
    public interface ICommand { }
}