﻿using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Commands {
    /// <summary>
    /// Предоставляет методы для выполнения любых команд
    /// </summary>
    public interface ICommandExecutor {
        /// <summary>
        /// Выполняет переданную команду
        /// </summary>
        /// <param name="command">Команда</param>
        /// <typeparam name="TCommand">Тип команды</typeparam>
        void Execute<TCommand>(TCommand command) where TCommand : ICommand;

        /// <summary>
        /// Выполняет переданную команду и возвращает результат
        /// </summary>
        /// <param name="command">Команда</param>
        /// <typeparam name="TCommand">Тип команды</typeparam>
        /// <typeparam name="TResult">Тип результата</typeparam>
        /// <returns>Результат выполнения команды</returns>
        TResult Execute<TCommand, TResult>(TCommand command) where TCommand : ICommand;

        /// <summary>
        /// Выполняет переданную команду асинхронно
        /// </summary>
        /// <param name="command">Команда</param>
        /// <param name="cancellationToken">Маркер отмены задачи</param>
        /// <typeparam name="TCommand">Тип команды</typeparam>
        /// <returns><see cref="Task" /></returns>
        Task ExecuteAsync<TCommand>(TCommand command, CancellationToken cancellationToken = default) where TCommand : ICommand;

        /// <summary>
        /// Выполняет переданную команду асинхронно и возвращает результат
        /// </summary>
        /// <param name="command">Команда</param>
        /// <param name="cancellationToken">Маркер отмены задачи</param>
        /// <typeparam name="TCommand">Тип команды</typeparam>
        /// <typeparam name="TResult">Тип результата</typeparam>
        /// <returns>Результат выполнения команды <see cref="Task{TResult}" /></returns>
        Task<TResult> ExecuteAsync<TCommand, TResult>(TCommand command, CancellationToken cancellationToken = default) where TCommand : ICommand;
    }
}