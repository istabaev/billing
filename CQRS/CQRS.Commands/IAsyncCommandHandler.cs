﻿using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Commands {
    /// <summary>
    /// Предоставляет механизм для асинхронного выполнения команд
    /// </summary>
    /// <typeparam name="TCommand">Тип команды</typeparam>
    public interface IAsyncCommandHandler<in TCommand> where TCommand : ICommand {
        /// <summary>
        /// Выполняет переданную команду асинхронно
        /// </summary>
        /// <param name="command">Команда</param>
        /// <param name="cancellationToken">Маркер отмены задачи</param>
        /// <returns><see cref="Task" /></returns>
        Task HandleAsync(TCommand command, CancellationToken cancellationToken = default);
    }

    /// <summary>
    /// Предоставляет механизм для асинхронного выполнения команд и возвращения результата
    /// </summary>
    /// <typeparam name="TCommand">Тип команды</typeparam>
    /// <typeparam name="TResult">Тип результата</typeparam>
    public interface IAsyncCommandHandler<in TCommand, TResult> where TCommand : ICommand {
        /// <summary>
        /// Выполняет переданную команду асинхронно и возвращает результат
        /// </summary>
        /// <param name="command">Команда</param>
        /// <param name="cancellationToken">Маркер отмены задачи</param>
        /// <returns>Результат выполнения команды <see cref="Task{TResult}" /></returns>
        Task<TResult> HandleAsync(TCommand command, CancellationToken cancellationToken = default);
    }
}