﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.DateTimes;
using Billing.Domain.Transactions;
using Billing.Repositories;
using Billing.Repositories.Transactions;
using Billing.Utilities.Providers.Contracts;
using UOW;

namespace Billing.Commands.Transactions {
    /// <summary>
    /// Обработчик команды подтверждения транзакции
    /// </summary>
    public class AcceptTransactionCommandHandler : AsyncReturnableCommandHandler<AcceptTransactionCommand, bool> {
        private readonly IDateTimeProvider _dateTimeProvider;

        public AcceptTransactionCommandHandler(IUnitOfWorkFactory unitOfWorkFactory, IDateTimeProvider dateTimeProvider) : base(unitOfWorkFactory) {
            _dateTimeProvider = dateTimeProvider;
        }

        protected override async Task<Func<bool>> OnHandleAsync(IBillingRepositories repositories,
                                                                AcceptTransactionCommand command,
                                                                CancellationToken cancellationToken = default) {
            GetTransactionCriteria criteria = new GetTransactionCriteria {
                TransactionId = command.TransactionId,
                AccountId = command.AccountId,
                UserId = command.UserId
            };
            Transaction transaction = await repositories.Transactions.GetAsync(criteria, cancellationToken);
            bool result = transaction.Accept(_dateTimeProvider.Now.ToTime(), command.TransactionDate);

            return () => result;
        }
    }
}