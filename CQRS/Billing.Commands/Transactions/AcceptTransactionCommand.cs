﻿using Billing.Domain.DateTimes;
using CQRS.Commands;

namespace Billing.Commands.Transactions {
    /// <summary>
    /// Команда подтверждения транзакции
    /// </summary>
    public class AcceptTransactionCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Идентификатор счета
        /// </summary>
        public long AccountId { get; set; }

        /// <summary>
        /// Идентификатор транзакции
        /// </summary>
        public long TransactionId { get; set; }

        /// <summary>
        /// Дата проведения транзакции
        /// </summary>
        public Date TransactionDate { get; set; }
    }
}