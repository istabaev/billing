﻿using Billing.Domain.Amounts;
using Billing.Domain.Transactions;
using CQRS.Commands;

namespace Billing.Commands.Transactions {
    /// <summary>
    /// Команда перевода средств между счетами
    /// </summary>
    public class TransferBetweenAccountsCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Счет, откуда снимаем средства
        /// </summary>
        public long FromAccountId { get; set; }

        /// <summary>
        /// Счет, куда зачисляем средства
        /// </summary>
        public long ToAccountId { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public Amount Amount { get; set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public TransactionType Type { get; set; }

        /// <summary>
        /// Описание транзакции
        /// </summary>
        public TransactionDescription Description { get; set; }
    }
}