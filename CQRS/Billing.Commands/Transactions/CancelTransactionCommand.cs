﻿using CQRS.Commands;

namespace Billing.Commands.Transactions {
    /// <summary>
    /// Команда отмены транзакции
    /// </summary>
    public class CancelTransactionCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Идентификатор счета
        /// </summary>
        public long AccountId { get; set; }

        /// <summary>
        /// Идентификатор транзакции
        /// </summary>
        public long TransactionId { get; set; }
    }
}