﻿using Billing.Domain.Amounts;
using Billing.Domain.DateTimes;
using Billing.Domain.Transactions;
using CQRS.Commands;

namespace Billing.Commands.Transactions {
    /// <summary>
    /// Команда создания транзакции по счету
    /// </summary>
    public class CreateTransactionCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Идентификатор счета
        /// </summary>
        public long AccountId { get; set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public TransactionType Type { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public TransactionStatus Status { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public Amount Amount { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public TransactionDescription Description { get; set; }

        /// <summary>
        /// Дата проведения транзакции
        /// </summary>
        public Date TransactionDate { get; set; }
    }
}