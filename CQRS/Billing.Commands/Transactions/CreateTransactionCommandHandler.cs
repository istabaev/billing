﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Accounts;
using Billing.Domain.DateTimes;
using Billing.Domain.Transactions;
using Billing.Domain.Transactions.Contracts;
using Billing.Repositories;
using Billing.Repositories.Accounts;
using Billing.Utilities.Providers.Contracts;
using UOW;

namespace Billing.Commands.Transactions {
    /// <summary>
    /// Обработчик команды создания транзакции по счету
    /// </summary>
    public class CreateTransactionCommandHandler : AsyncReturnableCommandHandler<CreateTransactionCommand, long> {
        private readonly IDateTimeProvider _dateTimeProvider;

        public CreateTransactionCommandHandler(IUnitOfWorkFactory unitOfWorkFactory, IDateTimeProvider dateTimeProvider) : base(unitOfWorkFactory) {
            _dateTimeProvider = dateTimeProvider;
        }

        protected override async Task<Func<long>> OnHandleAsync(IBillingRepositories repositories,
                                                                CreateTransactionCommand command,
                                                                CancellationToken cancellationToken = default) {
            GetAccountCriteria criteria = new GetAccountCriteria {
                AccountId = command.AccountId,
                UserId = command.UserId
            };
            Account account = await repositories.Accounts.GetAsync(criteria, cancellationToken);
            Transaction transaction = account.AddTransaction(new CreateTransactionParams {
                Amount = command.Amount,
                Type = command.Type,
                Description = command.Description,
                Now = _dateTimeProvider.Now.ToTime()
            });
            if (command.Status == TransactionStatus.Reserved) {
                transaction.Reserve(_dateTimeProvider.Now.ToTime());
            } else if (command.Status == TransactionStatus.Cancelled) {
                transaction.Cancel(_dateTimeProvider.Now.ToTime());
            } else if (command.Status == TransactionStatus.Accepted) {
                transaction.Accept(_dateTimeProvider.Now.ToTime(), command.TransactionDate);
            }

            return () => transaction.Id;
        }
    }
}