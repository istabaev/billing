﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Accounts;
using Billing.Domain.Amounts;
using Billing.Domain.Common.Extensions;
using Billing.Domain.DateTimes;
using Billing.Domain.Transactions;
using Billing.Domain.Transactions.Contracts;
using Billing.Domain.Users;
using Billing.Repositories;
using Billing.Repositories.Users;
using Billing.Utilities.Providers.Contracts;
using UOW;

namespace Billing.Commands.Transactions {
    /// <summary>
    /// Обработчик команды перевода средств между счетами
    /// </summary>
    public class TransferBetweenAccountsCommandHandler : AsyncCommandHandler<TransferBetweenAccountsCommand> {
        private readonly IDateTimeProvider _dateTimeProvider;

        public TransferBetweenAccountsCommandHandler(IUnitOfWorkFactory unitOfWorkFactory, IDateTimeProvider dateTimeProvider) : base(unitOfWorkFactory) {
            _dateTimeProvider = dateTimeProvider;
        }

        protected override async Task OnHandleAsync(IBillingRepositories repositories,
                                                    TransferBetweenAccountsCommand command,
                                                    CancellationToken cancellationToken = default) {
            GetUserCriteria criteria = new GetUserCriteria {
                UserId = command.UserId
            };
            User user = await repositories.Users.GetAsync(criteria, cancellationToken);
            await repositories.Users.LoadAccountsAsync(user, new [] { command.FromAccountId, command.ToAccountId }, cancellationToken);
            Account fromAccount = user.Accounts.GetById(command.FromAccountId) ?? throw new ArgumentNullException(nameof(command.FromAccountId));
            Account toAccount = user.Accounts.GetById(command.ToAccountId) ?? throw new ArgumentNullException(nameof(command.ToAccountId));

            Time time = _dateTimeProvider.Now.ToTime();
            Transaction writeOffTransaction = fromAccount.AddTransaction(new CreateTransactionParams {
                Amount = new Amount(-command.Amount.Value),
                Type = command.Type,
                Description = command.Description,
                Now = time
            });
            Transaction refillTransaction = toAccount.AddTransaction(new CreateTransactionParams {
                Amount = command.Amount,
                Type = command.Type,
                Description = command.Description,
                Now = time
            });

            writeOffTransaction.Accept(time);
            refillTransaction.Accept(time);
        }
    }
}