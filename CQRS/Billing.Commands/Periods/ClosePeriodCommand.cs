﻿using CQRS.Commands;

namespace Billing.Commands.Periods {
    /// <summary>
    /// Команда закрытия периода
    /// </summary>
    public class ClosePeriodCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Идентификатор счета
        /// </summary>
        public long AccountId { get; set; }
    }
}