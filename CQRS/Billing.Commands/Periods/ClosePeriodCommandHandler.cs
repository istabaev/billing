﻿using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Accounts;
using Billing.Domain.DateTimes;
using Billing.Repositories;
using Billing.Repositories.Accounts;
using Billing.Utilities.Providers.Contracts;
using UOW;

namespace Billing.Commands.Periods {
    /// <summary>
    /// Обработчик команды закрытия периода
    /// </summary>
    public class ClosePeriodCommandHandler : AsyncCommandHandler<ClosePeriodCommand> {
        private readonly IDateTimeProvider _dateTimeProvider;

        public ClosePeriodCommandHandler(IUnitOfWorkFactory unitOfWorkFactory, IDateTimeProvider dateTimeProvider) : base(unitOfWorkFactory) {
            _dateTimeProvider = dateTimeProvider;
        }

        protected override async Task OnHandleAsync(IBillingRepositories repositories,
                                                    ClosePeriodCommand command,
                                                    CancellationToken cancellationToken = default) {
            GetAccountCriteria criteria = new GetAccountCriteria {
                AccountId = command.AccountId,
                UserId = command.UserId
            };
            Account account = await repositories.Accounts.GetAsync(criteria, cancellationToken);
            account.CurrentPeriod.ClosePeriod(_dateTimeProvider.Now.ToTime());
        }
    }
}