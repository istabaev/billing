﻿using CQRS.Commands;

namespace Billing.Commands.Users {
    /// <summary>
    /// Команда блокировки пользователя
    /// </summary>
    public class BlockUserCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }
    }
}