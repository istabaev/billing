﻿using Billing.Domain.Users;
using CQRS.Commands;

namespace Billing.Commands.Users {
    /// <summary>
    /// Команда создания пользователя
    /// </summary>
    public class CreateUserCommand : ICommand {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public UserName Name { get; set; }

        /// <summary>
        /// Внешний пользователь
        /// </summary>
        public ExternalUser ExternalUser { get; set; }
    }
}