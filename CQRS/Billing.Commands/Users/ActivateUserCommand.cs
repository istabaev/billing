﻿using CQRS.Commands;

namespace Billing.Commands.Users {
    /// <summary>
    /// Команда активации пользователя
    /// </summary>
    public class ActivateUserCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }
    }
}