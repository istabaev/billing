﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.DateTimes;
using Billing.Domain.Users;
using Billing.Repositories;
using Billing.Repositories.Users;
using Billing.Utilities.Providers.Contracts;
using UOW;

namespace Billing.Commands.Users {
    /// <summary>
    /// Обработчик команды активации пользователя
    /// </summary>
    public class ActivateUserCommandHandler : AsyncReturnableCommandHandler<ActivateUserCommand, bool> {
        private readonly IDateTimeProvider _dateTimeProvider;
        public ActivateUserCommandHandler(IUnitOfWorkFactory unitOfWorkFactory, IDateTimeProvider dateTimeProvider) : base(unitOfWorkFactory) {
            _dateTimeProvider = dateTimeProvider;
        }

        protected override async Task<Func<bool>> OnHandleAsync(IBillingRepositories repositories,
                                                                ActivateUserCommand command,
                                                                CancellationToken cancellationToken = default) {
            GetUserCriteria criteria = new GetUserCriteria {
                UserId = command.UserId
            };
            User user = await repositories.Users.GetAsync(criteria, cancellationToken);
            bool result = user.Activate(_dateTimeProvider.Now.ToTime());

            return () => result;
        }
    }
}