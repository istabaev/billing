﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.DateTimes;
using Billing.Domain.Users;
using Billing.Repositories;
using Billing.Utilities.Providers.Contracts;
using UOW;

namespace Billing.Commands.Users {
    /// <summary>
    /// Обработчик команды создания пользователя
    /// </summary>
    public class CreateUserCommandHandler : AsyncReturnableCommandHandler<CreateUserCommand, long> {
        private readonly IDateTimeProvider _dateTimeProvider;

        public CreateUserCommandHandler(IUnitOfWorkFactory unitOfWorkFactory, IDateTimeProvider dateTimeProvider) : base(unitOfWorkFactory) {
            _dateTimeProvider = dateTimeProvider;
        }

        protected override async Task<Func<long>> OnHandleAsync(IBillingRepositories repositories,
                                                                CreateUserCommand command,
                                                                CancellationToken cancellationToken = default) {
            User user = new User(command.ExternalUser, command.Name, _dateTimeProvider.Now.ToTime());
            await repositories.Users.AddAsync(user, cancellationToken);

            return () => user.Id;
        }
    }
}