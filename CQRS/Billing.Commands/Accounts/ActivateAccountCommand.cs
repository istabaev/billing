﻿using CQRS.Commands;

namespace Billing.Commands.Accounts {
    /// <summary>
    /// Команда активации счета
    /// </summary>
    public class ActivateAccountCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Идентификатор счета
        /// </summary>
        public long AccountId { get; set; }
    }
}