﻿using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Accounts;
using Billing.Repositories;
using Billing.Repositories.Accounts;
using UOW;

namespace Billing.Commands.Accounts {
    /// <summary>
    /// Обработчик команды установки овердрафта счету
    /// </summary>
    public class SetAccountOverdraftCommandHandler : AsyncCommandHandler<SetAccountOverdraftCommand> {
        public SetAccountOverdraftCommandHandler(IUnitOfWorkFactory unitOfWorkFactory) : base(unitOfWorkFactory) { }

        protected override async Task OnHandleAsync(IBillingRepositories repositories,
                                                    SetAccountOverdraftCommand command,
                                                    CancellationToken cancellationToken = default) {
            GetAccountCriteria criteria = new GetAccountCriteria {
                AccountId = command.AccountId,
                UserId = command.UserId
            };
            Account account = await repositories.Accounts.GetAsync(criteria, cancellationToken);
            account.SetOverdraft(command.Overdraft);
        }
    }
}