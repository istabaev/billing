﻿using Billing.Domain.Accounts;
using Billing.Domain.Amounts;
using CQRS.Commands;

namespace Billing.Commands.Accounts {
    /// <summary>
    /// Команда создания счета пользователю
    /// </summary>
    public class CreateAccountCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Валюта счета
        /// </summary>
        public Currency Currency { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public AccountDescription Description { get; set; }
    }
}