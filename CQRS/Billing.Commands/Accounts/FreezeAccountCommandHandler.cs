﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Accounts;
using Billing.Domain.DateTimes;
using Billing.Repositories;
using Billing.Repositories.Accounts;
using Billing.Utilities.Providers.Contracts;
using UOW;

namespace Billing.Commands.Accounts {
    /// <summary>
    /// Обработчик команды заморозки счета
    /// </summary>
    public class FreezeAccountCommandHandler : AsyncReturnableCommandHandler<FreezeAccountCommand, bool> {
        private readonly IDateTimeProvider _dateTimeProvider;

        public FreezeAccountCommandHandler(IUnitOfWorkFactory unitOfWorkFactory, IDateTimeProvider dateTimeProvider) : base(unitOfWorkFactory) {
            _dateTimeProvider = dateTimeProvider;
        }

        protected override async Task<Func<bool>> OnHandleAsync(IBillingRepositories repositories,
                                                                FreezeAccountCommand command,
                                                                CancellationToken cancellationToken = default) {
            GetAccountCriteria criteria = new GetAccountCriteria {
                AccountId = command.AccountId,
                UserId = command.UserId
            };
            Account account = await repositories.Accounts.GetAsync(criteria, cancellationToken);
            bool result = account.Freeze(_dateTimeProvider.Now.ToTime());

            return () => result;
        }
    }
}