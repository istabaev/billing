﻿using Billing.Domain.Accounts;
using CQRS.Commands;

namespace Billing.Commands.Accounts {
    /// <summary>
    /// Команда установки овердрафта счету
    /// </summary>
    public class SetAccountOverdraftCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Идентификатор счета
        /// </summary>
        public long AccountId { get; set; }

        /// <summary>
        /// Овердрафт
        /// </summary>
        public AccountOverdraft Overdraft { get; set; }
    }
}