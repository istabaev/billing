﻿using CQRS.Commands;

namespace Billing.Commands.Accounts {
    /// <summary>
    /// Команда заморозки счета
    /// </summary>
    public class FreezeAccountCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Идентификатор счета
        /// </summary>
        public long AccountId { get; set; }
    }
}