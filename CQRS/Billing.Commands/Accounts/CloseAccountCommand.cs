﻿using CQRS.Commands;

namespace Billing.Commands.Accounts {
    /// <summary>
    /// Команда закрытия счета
    /// </summary>
    public class CloseAccountCommand : ICommand {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Идентификатор счета
        /// </summary>
        public long AccountId { get; set; }
    }
}