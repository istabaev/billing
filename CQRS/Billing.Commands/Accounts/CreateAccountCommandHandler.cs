﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Accounts;
using Billing.Domain.Accounts.Contracts;
using Billing.Domain.DateTimes;
using Billing.Domain.Users;
using Billing.Repositories;
using Billing.Repositories.Users;
using Billing.Utilities.Providers.Contracts;
using UOW;

namespace Billing.Commands.Accounts {
    /// <summary>
    /// Обработчик команды создания счета пользователю
    /// </summary>
    public class CreateAccountCommandHandler : AsyncReturnableCommandHandler<CreateAccountCommand, long> {
        private readonly IDateTimeProvider _dateTimeProvider;

        public CreateAccountCommandHandler(IUnitOfWorkFactory unitOfWorkFactory, IDateTimeProvider dateTimeProvider) : base(unitOfWorkFactory) {
            _dateTimeProvider = dateTimeProvider;
        }

        protected override async Task<Func<long>> OnHandleAsync(IBillingRepositories repositories,
                                                                CreateAccountCommand command,
                                                                CancellationToken cancellationToken = default) {
            GetUserCriteria criteria = new GetUserCriteria {
                UserId = command.UserId
            };
            User user = await repositories.Users.GetAsync(criteria, cancellationToken);
            Account account = user.AddAccount(new CreateAccountParams {
                Currency = command.Currency,
                Description = command.Description,
                Now = _dateTimeProvider.Now.ToTime()
            });

            return () => account.Id;
        }
    }
}