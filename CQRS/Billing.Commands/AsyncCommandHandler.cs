﻿using System.Threading;
using System.Threading.Tasks;
using Billing.Repositories;
using CQRS.Commands;
using UOW;

namespace Billing.Commands {
    /// <summary>
    /// Базовая реализация обработчика команд с возвращением результата с использованием паттерна "Единица работы" 
    /// </summary>
    /// <typeparam name="TCommand">Тип команды</typeparam>
    public abstract class AsyncCommandHandler<TCommand> : IAsyncCommandHandler<TCommand>
        where TCommand : ICommand {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        protected AsyncCommandHandler(IUnitOfWorkFactory unitOfWorkFactory) {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        /// <inheritdoc />
        public virtual async Task HandleAsync(TCommand command,
                                              CancellationToken cancellationToken = default) {
            await using var unitOfWork = _unitOfWorkFactory.Create<IBillingRepositories>();

            await OnHandleAsync(unitOfWork.Repository, command, cancellationToken);

            await unitOfWork.SaveAsync(cancellationToken);
        }

        /// <summary>
        /// Непосредственная обработка команды
        /// </summary>
        /// <param name="repositories">Репозитории</param>
        /// <param name="command">Команда</param>
        /// <param name="cancellationToken">Маркер отмены задачи</param>
        /// <returns>Функция для возвращения результата выполнения команды</returns>
        protected abstract Task OnHandleAsync(IBillingRepositories repositories,
                                              TCommand command,
                                              CancellationToken cancellationToken = default);
    }
}