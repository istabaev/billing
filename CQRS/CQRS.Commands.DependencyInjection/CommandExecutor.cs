﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace CQRS.Commands.DependencyInjection {
    /// <summary>
    /// Реализация <see cref="ICommandExecutor" /> с использованием <see cref="DependencyInjection" />
    /// </summary>
    public class CommandExecutor : ICommandExecutor {
        private readonly IServiceProvider _serviceProvider;

        public CommandExecutor(IServiceProvider serviceProvider) {
            _serviceProvider = serviceProvider;
        }

        /// <inheritdoc />
        public void Execute<TCommand>(TCommand command)
            where TCommand : ICommand {
            ICommandHandler<TCommand> handler = _serviceProvider.GetService<ICommandHandler<TCommand>>();
            ThrowIfHandlerNotFound(command, handler);
            handler.Handle(command);
        }

        /// <inheritdoc />
        public TResult Execute<TCommand, TResult>(TCommand command)
            where TCommand : ICommand {
            ICommandHandler<TCommand, TResult> handler = _serviceProvider.GetService<ICommandHandler<TCommand, TResult>>();
            ThrowIfHandlerNotFound(command, handler);
            return handler.Handle(command);
        }

        /// <inheritdoc />
        public async Task ExecuteAsync<TCommand>(TCommand command,
                                                 CancellationToken cancellationToken = default)
            where TCommand : ICommand {
            IAsyncCommandHandler<TCommand> handler = _serviceProvider.GetService<IAsyncCommandHandler<TCommand>>();
            ThrowIfHandlerNotFound(command, handler);
            await handler.HandleAsync(command, cancellationToken);
        }

        /// <inheritdoc />
        public async Task<TResult> ExecuteAsync<TCommand, TResult>(TCommand command,
                                                                   CancellationToken cancellationToken = default)
            where TCommand : ICommand {
            IAsyncCommandHandler<TCommand, TResult> handler = _serviceProvider.GetService<IAsyncCommandHandler<TCommand, TResult>>();
            ThrowIfHandlerNotFound(command, handler);
            return await handler.HandleAsync(command, cancellationToken);
        }

        private static void ThrowIfHandlerNotFound<TCommand, TCommandHandler>(TCommand command, TCommandHandler handler) {
            if (handler == null) {
                throw new ArgumentNullException($"Обработчик команды {typeof(TCommand).Name} не найден");
            }
        }
    }
}