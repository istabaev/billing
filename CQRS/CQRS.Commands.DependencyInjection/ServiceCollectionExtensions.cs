﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace CQRS.Commands.DependencyInjection {
    /// <summary>
    /// Методы расширения для регистрации обработчиков команд в <see cref="IServiceCollection" />
    /// </summary>
    public static class ServiceCollectionExtensions {
        /// <summary>
        /// Регистрирует все найденные в сборках обработчики команд в <see cref="IServiceCollection" />
        /// </summary>
        /// <param name="services">Коллекция <see cref="IServiceCollection" />, в которую добавляются сервисы</param>
        /// <param name="assemblies">Список сборок, в которых необходимо провести поиск</param>
        public static void AddCommands(this IServiceCollection services, Assembly[] assemblies) {
            services.AddScoped<ICommandExecutor, CommandExecutor>();

            Type[] interfaceTypes = new[] {
                typeof(ICommandHandler<>),
                typeof(ICommandHandler<,>),
                typeof(IAsyncCommandHandler<>),
                typeof(IAsyncCommandHandler<,>)
            };
            List<TypeInfo> implementationTypes = assemblies
                .SelectMany(a =>
                    a.DefinedTypes
                        .Where(x => !x.IsAbstract)
                        .Where(x =>
                            x.GetInterfaces()
                                .Where(i => i.IsGenericType)
                                .Select(i => i.GetGenericTypeDefinition())
                                .Intersect(interfaceTypes)
                                .Any()))
                .ToList();
            foreach (TypeInfo implementationType in implementationTypes) {
                List<Type> implementationInterfaceTypes = implementationType
                    .GetInterfaces()
                    .Where(i => i.IsGenericType)
                    .Where(i => interfaceTypes.Contains(i.GetGenericTypeDefinition()))
                    .ToList();
                foreach (Type interfaceType in implementationInterfaceTypes) {
                    services.AddTransient(interfaceType, implementationType);
                }
            }
        }
    }
}