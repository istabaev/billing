﻿using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Queries {
    public interface IAsyncQueryHandler<in TQuery, TResult> where TQuery : IQuery<TResult> {
        Task<TResult> AskAsync(TQuery query, CancellationToken cancellationToken = default);
    }
}