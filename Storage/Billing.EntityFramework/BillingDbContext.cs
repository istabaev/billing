﻿using Billing.Domain.Accounts;
using Billing.Domain.Periods;
using Billing.Domain.Transactions;
using Billing.Domain.Users;
using Microsoft.EntityFrameworkCore;

namespace Billing.EntityFramework {
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    public abstract class BillingDbContext : DbContext {
        protected BillingDbContext(DbContextOptions options)
            : base(options) { }

        /// <summary>
        /// Пользователи
        /// </summary>
        public DbSet<User> Users { get; internal set; }

        /// <summary>
        /// Счета
        /// </summary>
        public DbSet<Account> Accounts { get; internal set; }

        /// <summary>
        /// Транзакции
        /// </summary>
        public DbSet<Transaction> Transactions { get; internal set; }

        /// <summary>
        /// Периоды
        /// </summary>
        public DbSet<Period> Periods { get; internal set; }
    }
}