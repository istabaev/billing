﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Billing.EntityFramework.Converters {
    /// <summary>
    /// Фабрика конвертеров перечислений в строчный тип
    /// </summary>
    internal class EnumToStringConverterFactory {
        /// <summary>
        /// Возвращает экземпляр конвертера указанного перечисления в строчный тип
        /// </summary>
        /// <typeparam name="T">Тип перечисления</typeparam>
        /// <returns>Экземпляр конвертера</returns>
        public EnumToStringConverter<T> Get<T>() where T : struct {
            return new EnumToStringConverter<T>();
        }
    }
}