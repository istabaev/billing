﻿using Billing.EntityFramework.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Billing.EntityFramework {
    /// <summary>
    /// Контекст базы данных Postgresql
    /// </summary>
    public sealed class NpgsqlBillingDbContext : BillingDbContext {
        public NpgsqlBillingDbContext(DbContextOptions<NpgsqlBillingDbContext> options)
            : base(options) {
            ChangeTracker.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new AccountConfiguration());
            modelBuilder.ApplyConfiguration(new PeriodConfiguration());
            modelBuilder.ApplyConfiguration(new TransactionConfiguration());
        }
    }
}