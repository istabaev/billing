﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Billing.EntityFramework {
    internal class Program {
        private static readonly ManualResetEventSlim _done = new ManualResetEventSlim(false);
        private static readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        static Program() {
            Console.CancelKeyPress += ConsoleOnCancelKeyPress;
        }

        private static void ConsoleOnCancelKeyPress(object sender, ConsoleCancelEventArgs e) {
            if (!_cancellationTokenSource.IsCancellationRequested) {
                Console.WriteLine("Application is shutting down...");
                _cancellationTokenSource.Cancel();
            }

            _done.Wait();
            e.Cancel = true;
        }

        static async Task Main(string[] args) {
            using (_cancellationTokenSource) {
                Console.WriteLine("Configuring services...");
                var startup = new Startup();
                var services = new ServiceCollection();
                startup.ConfigureServices(services);
                var serviceProvider = services.BuildServiceProvider();

                Console.WriteLine("Starting migrations...");
                try {
                    var dbContext = serviceProvider.GetService<NpgsqlBillingDbContext>();
                    await dbContext.Database.MigrateAsync(_cancellationTokenSource.Token);
                    Console.WriteLine("Migrations finished.");
                } catch (OperationCanceledException) {
                    Console.WriteLine("Migrations canceled.");
                }

                if (_cancellationTokenSource.IsCancellationRequested) {
                    Console.WriteLine("Waiting cancel...");
                    _cancellationTokenSource.Token.WaitHandle.WaitOne();
                }

                _done.Set();

                Console.CancelKeyPress -= ConsoleOnCancelKeyPress;

                Console.WriteLine("Done");
            }
        }
    }
}