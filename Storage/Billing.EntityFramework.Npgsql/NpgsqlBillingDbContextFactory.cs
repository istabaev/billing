﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;

namespace Billing.EntityFramework {
    /// <summary>
    /// Фабрика для создания контекста базы данных при генерации миграций
    /// </summary>
    /// <remarks>Не удалять!</remarks>
    internal sealed class NpgsqlBillingDbContextFactory : IDesignTimeDbContextFactory<NpgsqlBillingDbContext>
    {
        /// <inheritdoc />
        public NpgsqlBillingDbContext CreateDbContext(string[] args)
        {
            var startup = new Startup();
            var services = new ServiceCollection();
            startup.ConfigureServices(services);
            var serviceProvider = services.BuildServiceProvider();

            var dbContext = serviceProvider.GetService<NpgsqlBillingDbContext>();
            return dbContext;
        }
    }
}