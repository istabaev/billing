﻿using System;
using System.Linq.Expressions;
using Billing.Domain.Amounts;
using Billing.Domain.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Billing.EntityFramework.Extensions {
    /// <summary>
    /// Методы расширения для <see cref="OwnedNavigationBuilder{TEntity,TDependentEntity}" />
    /// </summary>
    public static class OwnedNavigationBuilderExtensions {
        /// <summary>
        /// Настраивает билдер для конвертации значения
        /// </summary>
        /// <param name="builder">Билдер модели</param>
        /// <param name="expression">Выражение свойства</param>
        /// <param name="isRequired">Обязательность заполнения значения</param>
        /// <param name="columnName">Название колонки</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TOwnedEntity">Тип свойства обертки</typeparam>
        /// <typeparam name="TDependentEntity">Тип свойства значения</typeparam>
        /// <typeparam name="TValue">Тип значения</typeparam>
        /// <returns>Билдер свойства</returns>
        public static PropertyBuilder<TValue> Value<TEntity, TOwnedEntity, TDependentEntity, TValue>(
            this OwnedNavigationBuilder<TEntity, TOwnedEntity> builder,
            Expression<Func<TOwnedEntity, TDependentEntity>> expression,
            bool isRequired,
            string columnName = null)
            where TEntity : class
            where TOwnedEntity : class
            where TDependentEntity : ValueObject<TDependentEntity, TValue> {
            PropertyBuilder<TValue> propertyBuilder = null;
            builder.OwnsOne(expression, a => {
                propertyBuilder = a.Property(v => v.Value)
                    .HasColumnName(columnName ?? ((MemberExpression)expression.Body).Member.Name);
                if (isRequired) {
                    propertyBuilder = propertyBuilder.IsRequired();
                }
            });
            if (isRequired) {
                builder.Navigation(expression).IsRequired();
            }

            return propertyBuilder;
        }

        /// <summary>
        /// Настраивает билдер для конвертации строкового значения
        /// </summary>
        /// <param name="builder">Билдер модели</param>
        /// <param name="expression">Выражение свойства</param>
        /// <param name="maxLength">Максимальная длина строки</param>
        /// <param name="isRequired">Обязательность заполнения значения</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TOwnedEntity">Тип свойства обертки</typeparam>
        /// <typeparam name="TDependentEntity">Тип свойства значения</typeparam>
        /// <returns>Билдер свойства</returns>
        public static PropertyBuilder<string> StringValue<TEntity, TOwnedEntity, TDependentEntity>(
            this OwnedNavigationBuilder<TEntity, TOwnedEntity> builder,
            Expression<Func<TOwnedEntity, TDependentEntity>> expression,
            int maxLength,
            bool isRequired)
            where TEntity : class
            where TOwnedEntity : class
            where TDependentEntity : ValueObject<TDependentEntity, string> {
            PropertyBuilder<string> propertyBuilder = Value<TEntity, TOwnedEntity, TDependentEntity, string>(builder, expression, isRequired);
            return propertyBuilder.HasMaxLength(maxLength);
        }

        /// <summary>
        /// Настраивает билдер для конвертации суммы
        /// </summary>
        /// <param name="builder">Билдер модели</param>
        /// <param name="expression">Выражение свойства</param>
        /// <param name="isRequired">Обязательность заполнения значения</param>
        /// <param name="columnName">Название колонки</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TOwnedEntity">Тип свойства обертки</typeparam>
        /// <typeparam name="TAmount">Тип суммы</typeparam>
        /// <returns>Билдер свойства</returns>
        public static PropertyBuilder<decimal> AmountValue<TEntity, TOwnedEntity, TAmount>(
            this OwnedNavigationBuilder<TEntity, TOwnedEntity> builder,
            Expression<Func<TOwnedEntity, TAmount>> expression,
            bool isRequired,
            string columnName = null)
            where TEntity : class
            where TOwnedEntity : class
            where TAmount : Amount<TAmount> {
            return Value<TEntity, TOwnedEntity, TAmount, decimal>(builder, expression, isRequired, columnName);
        }
    }
}