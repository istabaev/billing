﻿using System;
using System.Linq.Expressions;
using Billing.Domain.Amounts;
using Billing.Domain.Common;
using Billing.Domain.DateTimes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using EnumerationClass = Billing.Domain.Common.Enumeration;

namespace Billing.EntityFramework.Extensions {
    /// <summary>
    /// Методы расширения для <see cref="EntityTypeBuilder{TEntity}" />
    /// </summary>
    internal static class EntityBuilderExtensions {
        /// <summary>
        /// Настраивает модель для конвертации перечисления <see cref="EnumerationClass"/>
        /// </summary>
        /// <param name="builder">Билдер модели</param>
        /// <param name="expression">Выражение свойства</param>
        /// <param name="maxLength">Максимальная длина строки</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TRelatedEntity">Тип свойства</typeparam>
        /// <returns>Билдер свойства</returns>
        public static PropertyBuilder<TRelatedEntity> Enumeration<TEntity, TRelatedEntity>(
            this EntityTypeBuilder<TEntity> builder,
            Expression<Func<TEntity, TRelatedEntity>> expression,
            int maxLength)
            where TEntity : class
            where TRelatedEntity : Enumeration {
            PropertyBuilder<TRelatedEntity> propertyBuilder = builder
                .Property(expression)
                .HasConversion(x => x.Name, x => EnumerationClass.FromName<TRelatedEntity>(x))
                .HasMaxLength(maxLength)
                .IsRequired();

            return propertyBuilder;
        }

        /// <summary>
        /// Настраивает билдер для конвертации значения
        /// </summary>
        /// <param name="builder">Билдер модели</param>
        /// <param name="expression">Выражение свойства</param>
        /// <param name="isRequired">Обязательность заполнения значения</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TRelatedEntity">Тип свойства</typeparam>
        /// <typeparam name="TValue">Тип значения</typeparam>
        /// <returns>Билдер свойства</returns>
        public static PropertyBuilder<TValue> Value<TEntity, TRelatedEntity, TValue>(
            this EntityTypeBuilder<TEntity> builder,
            Expression<Func<TEntity, TRelatedEntity>> expression,
            bool isRequired)
            where TEntity : class
            where TRelatedEntity : ValueObject<TRelatedEntity, TValue> {
            PropertyBuilder<TValue> propertyBuilder = null;
            builder.OwnsOne(expression, a => {
                propertyBuilder = a.Property(v => v.Value)
                    .HasColumnName(((MemberExpression)expression.Body).Member.Name);
                if (isRequired) {
                    propertyBuilder = propertyBuilder.IsRequired();
                }
            });
            if (isRequired) {
                builder.Navigation(expression).IsRequired();
            }

            return propertyBuilder;
        }

        /// <summary>
        /// Настраивает модель для конвертации строкового значения
        /// </summary>
        /// <param name="builder">Билдер модели</param>
        /// <param name="expression">Выражение свойства</param>
        /// <param name="maxLength">Максимальная длина строки</param>
        /// <param name="isRequired">Обязательность заполнения значения</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TRelatedEntity">Тип свойства</typeparam>
        /// <returns>Билдер свойства</returns>
        public static PropertyBuilder<string> StringValue<TEntity, TRelatedEntity>(
            this EntityTypeBuilder<TEntity> builder,
            Expression<Func<TEntity, TRelatedEntity>> expression,
            int maxLength,
            bool isRequired)
            where TEntity : class
            where TRelatedEntity : ValueObject<TRelatedEntity, string> {
            PropertyBuilder<string> propertyBuilder = Value<TEntity, TRelatedEntity, string>(builder, expression, isRequired);
            return propertyBuilder.HasMaxLength(maxLength);
        }

        /// <summary>
        /// Настраивает билдер для конвертации даты
        /// </summary>
        /// <param name="builder">Билдер модели</param>
        /// <param name="expression">Выражение свойства</param>
        /// <param name="isRequired">Обязательность заполнения значения</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <returns>Билдер свойства</returns>
        public static PropertyBuilder<DateTime> DateValue<TEntity>(
            this EntityTypeBuilder<TEntity> builder,
            Expression<Func<TEntity, Date>> expression,
            bool isRequired)
            where TEntity : class {
            return Value<TEntity, Date, DateTime>(builder, expression, isRequired).HasColumnType("date");
        }

        /// <summary>
        /// Настраивает билдер для конвертации даты и времени
        /// </summary>
        /// <param name="builder">Билдер модели</param>
        /// <param name="expression">Выражение свойства</param>
        /// <param name="isRequired">Обязательность заполнения значения</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <returns>Билдер свойства</returns>
        public static PropertyBuilder<DateTime> TimeValue<TEntity>(
            this EntityTypeBuilder<TEntity> builder,
            Expression<Func<TEntity, Time>> expression,
            bool isRequired)
            where TEntity : class {
            return Value<TEntity, Time, DateTime>(builder, expression, isRequired).HasColumnType("timestamp without time zone");
        }

        /// <summary>
        /// Настраивает билдер для конвертации суммы
        /// </summary>
        /// <param name="builder">Билдер модели</param>
        /// <param name="expression">Выражение свойства</param>
        /// <param name="isRequired">Обязательность заполнения значения</param>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TAmount">Тип суммы</typeparam>
        /// <returns>Билдер свойства</returns>
        public static PropertyBuilder<decimal> AmountValue<TEntity, TAmount>(
            this EntityTypeBuilder<TEntity> builder,
            Expression<Func<TEntity, TAmount>> expression,
            bool isRequired)
            where TEntity : class
            where TAmount : Amount<TAmount> {
            return Value<TEntity, TAmount, decimal>(builder, expression, isRequired);
        }
    }
}