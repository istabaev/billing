﻿using System.ComponentModel;
using Billing.EntityFramework.Converters;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Billing.EntityFramework.Extensions {
    /// <summary>
    /// Методы расширения для <see cref="PropertyBuilder{TProperty}" />
    /// </summary>
    internal static class PropertyBuilderExtensions {
        private static readonly EnumToStringConverterFactory _enumToString;

        static PropertyBuilderExtensions() {
            _enumToString = new EnumToStringConverterFactory();
        }

        /// <summary>
        /// Настраивает свойство модели для конвертации значения из перечисления в строку
        /// </summary>
        /// <param name="builder">Билдер свойства модели</param>
        /// <param name="maxLength">Максимальная длина строки</param>
        /// <typeparam name="TProperty">Тип свойства</typeparam>
        /// <returns>Билдер свойства модели</returns>
        /// <exception cref="InvalidEnumArgumentException"></exception>
        public static PropertyBuilder<TProperty> IsStringEnum<TProperty>(
            this PropertyBuilder<TProperty> builder,
            int? maxLength = null)
            where TProperty : struct {
            if (!typeof(TProperty).IsEnum) {
                throw new InvalidEnumArgumentException();
            }

            builder = builder.HasConversion(_enumToString.Get<TProperty>());
            if (maxLength.HasValue) {
                builder = builder.HasMaxLength(maxLength.Value);
            }
            return builder;
        }
    }
}