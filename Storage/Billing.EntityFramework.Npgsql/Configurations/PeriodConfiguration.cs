﻿using Billing.Domain.Periods;
using Billing.EntityFramework.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Billing.EntityFramework.Configurations {
    internal class PeriodConfiguration : IEntityTypeConfiguration<Period> {
        public void Configure(EntityTypeBuilder<Period> builder) {
            builder.ToTable("period");
            builder.HasKey(x => x.Id);

            builder
                .HasOne(x => x.User)
                .WithMany()
                .HasForeignKey("UserId")
                .HasPrincipalKey(x => x.Id)
                .IsRequired();
            builder.Navigation(x => x.User).IsRequired();

            builder
                .HasOne(x => x.Account)
                .WithMany(x => x.Periods)
                .HasForeignKey("AccountId")
                .HasPrincipalKey(x => x.Id)
                .IsRequired();
            builder.Navigation(x => x.Account).IsRequired();

            builder.Enumeration(x => x.Status, 16);

            builder.OwnsOne(x => x.StartBalance, a => {
                a.AmountValue(x => x.Balance, true, "StartBalance").HasDefaultValue(decimal.Zero);
                a.AmountValue(x => x.Reserve, true, "StartReserve").HasDefaultValue(decimal.Zero);
            });
            builder.Navigation(x => x.StartBalance).IsRequired();
            builder.OwnsOne(x => x.EndBalance, a => {
                a.AmountValue(x => x.Balance, true, "EndBalance").HasDefaultValue(decimal.Zero);
                a.AmountValue(x => x.Reserve, true, "EndReserve").HasDefaultValue(decimal.Zero);
            });
            builder.Navigation(x => x.EndBalance).IsRequired();

            builder.DateValue(x => x.StartDate, true);
            builder.DateValue(x => x.EndDate, false);
            builder.TimeValue(x => x.CreatedTime, true);
            builder.TimeValue(x => x.ClosedTime, false);
        }
    }
}