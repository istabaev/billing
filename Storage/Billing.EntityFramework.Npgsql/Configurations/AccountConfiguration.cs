﻿using Billing.Domain.Accounts;
using Billing.EntityFramework.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Billing.EntityFramework.Configurations {
    internal class AccountConfiguration : IEntityTypeConfiguration<Account> {
        public void Configure(EntityTypeBuilder<Account> builder) {
            builder.ToTable("account");
            builder.HasKey(x => x.Id);

            builder
                .HasOne(x => x.User)
                .WithMany(x => x.Accounts)
                .HasForeignKey("UserId")
                .HasPrincipalKey(x => x.Id)
                .IsRequired();
            builder.Navigation(x => x.User).IsRequired();

            builder.Enumeration(x => x.Status, 16);

            builder.OwnsOne(x => x.Balance, a => {
                a.AmountValue(x => x.Balance, true).HasDefaultValue(decimal.Zero);
                a.AmountValue(x => x.Reserve, true).HasDefaultValue(decimal.Zero);
            });
            builder.Navigation(x => x.Balance).IsRequired();

            builder.AmountValue(x => x.Overdraft, true).HasDefaultValue(decimal.Zero);
            builder.Enumeration(x => x.Currency, 3);
            builder.StringValue(x => x.Description, AccountDescription.MAX_LENGTH, true);
            builder.TimeValue(x => x.CreatedTime, true);
            builder.TimeValue(x => x.ActivatedTime, false);
            builder.TimeValue(x => x.FrozenTime, false);
            builder.TimeValue(x => x.ClosedTime, false);
        }
    }
}