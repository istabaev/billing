﻿using Billing.Domain.Users;
using Billing.EntityFramework.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Billing.EntityFramework.Configurations {
    internal class UserConfiguration : IEntityTypeConfiguration<User> {
        public void Configure(EntityTypeBuilder<User> builder) {
            builder.ToTable("user");
            builder.HasKey(x => x.Id);

            builder.OwnsOne(x => x.ExternalUser, a => {
                a.StringValue(e => e.Product, Product.MAX_LENGTH, true);
                a.StringValue(e => e.ExternalId, ExternalId.MAX_LENGTH, true);
                // a.HasIndex(nameof(ExternalUser.Product), nameof(ExternalUser.ExternalId)).IsUnique();
            });
            builder.Navigation(x => x.ExternalUser).IsRequired();

            builder.StringValue(x => x.Name, UserName.MAX_LENGTH, true);
            builder.Enumeration(x => x.Status, 16);
            builder.TimeValue(x => x.CreatedTime, true);
            builder.TimeValue(x => x.ActivatedTime, false);
            builder.TimeValue(x => x.BlockedTime, false);
        }
    }
}