﻿using Billing.Domain.Transactions;
using Billing.EntityFramework.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Billing.EntityFramework.Configurations {
    internal class TransactionConfiguration : IEntityTypeConfiguration<Transaction> {
        public void Configure(EntityTypeBuilder<Transaction> builder) {
            builder.ToTable("transaction");
            builder.HasKey(x => x.Id);

            builder
                .HasOne(x => x.User)
                .WithMany()
                .HasForeignKey("UserId")
                .HasPrincipalKey(x => x.Id)
                .IsRequired();
            builder.Navigation(x => x.User).IsRequired();

            builder
                .HasOne(x => x.Account)
                .WithMany()
                .HasForeignKey("AccountId")
                .HasPrincipalKey(x => x.Id)
                .IsRequired();
            builder.Navigation(x => x.Account).IsRequired();

            builder
                .HasOne(x => x.Period)
                .WithMany(x => x.Transactions)
                .HasForeignKey("PeriodId")
                .HasPrincipalKey(x => x.Id)
                .IsRequired();
            builder.Navigation(x => x.Period).IsRequired();

            builder.Enumeration(x => x.Status, 16);
            builder.AmountValue(x => x.Amount, true);
            builder.Enumeration(x => x.Currency, 3);
            builder.StringValue(x => x.Type, 32, true);
            builder.StringValue(x => x.Description, 256, true);
            builder.DateValue(x => x.Date, false);
            builder.TimeValue(x => x.CreatedTime, true);
            builder.TimeValue(x => x.ReservedTime, false);
            builder.TimeValue(x => x.AcceptedTime, false);
            builder.TimeValue(x => x.CancelledTime, false);
        }
    }
}