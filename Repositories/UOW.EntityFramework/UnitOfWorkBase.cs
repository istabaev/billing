﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace UOW.EntityFramework {
    /// <summary>
    /// Реализация паттерна "Единица работы" с использованием Entity Framework
    /// </summary>
    /// <typeparam name="TDbContext">Тип контекста базы данных</typeparam>
    /// <typeparam name="TRepository">Тип репозитория</typeparam>
    public abstract class UnitOfWorkBase<TDbContext, TRepository> : IUnitOfWork<TRepository> where TDbContext : DbContext {
        private readonly TDbContext _context;
        private bool _disposed;

        protected UnitOfWorkBase(TDbContext context, TRepository repository) {
            _context = context;
            Repository = repository;
        }

        /// <inheritdoc />
        public TRepository Repository { get; }

        /// <inheritdoc />
        public async Task SaveAsync(CancellationToken cancellationToken = default) {
            await _context.SaveChangesAsync(cancellationToken);
        }

        /// <inheritdoc />
        public void Dispose() {
            DisposeInternal();
            GC.SuppressFinalize(this);
        }

        /// <inheritdoc />
        public async ValueTask DisposeAsync() {
            await DisposeInternalAsync();
            GC.SuppressFinalize(this);
        }

        private void DisposeInternal() {
            if (!_disposed) {
                _context.Dispose();
            }

            _disposed = true;
        }

        private async Task DisposeInternalAsync() {
            if (!_disposed) {
                await _context.DisposeAsync();
            }

            _disposed = true;
        }
    }
}