﻿namespace UOW {
    /// <summary>
    /// Предоставляет фабрику по созданию экземпляров "Единицы работы"
    /// </summary>
    public interface IUnitOfWorkFactory {
        /// <summary>
        /// Создает экземпляр "Единицы работы"
        /// </summary>
        /// <typeparam name="TRepository">Тип репозитория</typeparam>
        IUnitOfWork<TRepository> Create<TRepository>();
    }
}