﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace UOW {
    /// <summary>
    /// Интерфейс для реализации паттерна "Единица работы"
    /// </summary>
    /// <typeparam name="TRepository">Тип репозитория</typeparam>
    public interface IUnitOfWork<out TRepository> : IDisposable, IAsyncDisposable {
        /// <summary>
        /// Репозиторий доступа к данным
        /// </summary>
        TRepository Repository { get; }

        /// <summary>
        /// Выполняет сохранение изменений асинхронно
        /// </summary>
        /// <param name="cancellationToken">Маркер отмены задачи</param>
        Task SaveAsync(CancellationToken cancellationToken = default);
    }
}