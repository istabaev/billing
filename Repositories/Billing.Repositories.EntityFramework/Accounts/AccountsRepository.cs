﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Accounts;
using Billing.EntityFramework;
using Billing.Repositories.Accounts;
using Microsoft.EntityFrameworkCore;

namespace Billing.Repositories.EntityFramework.Accounts {
    /// <summary>
    /// Репозиторий счетов
    /// </summary>
    public class AccountsRepository : IAccountsRepository {
        private readonly BillingDbContext _dbContext;

        public AccountsRepository(BillingDbContext dbContext) {
            _dbContext = dbContext;
        }

        /// <inheritdoc />
        public async Task<Account> GetAsync(GetAccountCriteria criteria, CancellationToken cancellationToken = default) {
            Account account = await _dbContext.Accounts
                .Include(x => x.User)
                .Include(x => x.Periods.OrderByDescending(p => p.CreatedTime.Value).Take(1))
                    .ThenInclude(x => x.Transactions)
                .Where(x => x.Id == criteria.AccountId)
                .Where(x => criteria.UserId == null || x.User.Id == criteria.UserId)
                .SingleOrDefaultAsync(cancellationToken);
            return account;
        }
    }
}