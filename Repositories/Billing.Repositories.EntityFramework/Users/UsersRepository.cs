﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Users;
using Billing.Repositories.Users;
using Billing.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Billing.Repositories.EntityFramework.Users {
    /// <summary>
    /// Репозиторий пользователей
    /// </summary>
    public class UsersRepository : IUsersRepository {
        private readonly BillingDbContext _dbContext;

        public UsersRepository(BillingDbContext dbContext) {
            _dbContext = dbContext;
        }

        /// <inheritdoc />
        public async Task<User> GetAsync(GetUserCriteria criteria, CancellationToken cancellationToken = default) {
            User user = await _dbContext.Users
                .SingleOrDefaultAsync(x => x.Id == criteria.UserId, cancellationToken);
            return user;
        }

        /// <inheritdoc />
        public async Task<User> AddAsync(User user, CancellationToken cancellationToken = default) {
            EntityEntry<User> entry = await _dbContext.Users.AddAsync(user, cancellationToken);
            return entry.Entity;
        }

        /// <inheritdoc />
        public async Task LoadAccountsAsync(User user, long[] accountIds, CancellationToken cancellationToken = default) {
            await _dbContext.Entry(user)
                .Collection(x => x.Accounts)
                .Query()
                .Include(x => x.Periods.OrderByDescending(p => p.CreatedTime.Value).Take(1))
                    .ThenInclude(x => x.Transactions)
                .Where(x => accountIds.Contains(x.Id))
                .LoadAsync(cancellationToken);
        }
    }
}