﻿using Billing.EntityFramework;
using UOW.EntityFramework;

namespace Billing.Repositories.EntityFramework {
    /// <summary>
    /// Реализация паттерна "Единица работы"
    /// </summary>
    public class UnitOfWork : UnitOfWorkBase<BillingDbContext, IBillingRepositories> {
        public UnitOfWork(BillingDbContext context, IBillingRepositories repository) : base(context, repository) { }
    }
}