﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Transactions;
using Billing.EntityFramework;
using Billing.Repositories.Transactions;
using Microsoft.EntityFrameworkCore;

namespace Billing.Repositories.EntityFramework.Transactions {
    /// <summary>
    /// Репозиторий транзакций
    /// </summary>
    public class TransactionsRepository : ITransactionsRepository {
        private readonly BillingDbContext _dbContext;

        public TransactionsRepository(BillingDbContext dbContext) {
            _dbContext = dbContext;
        }

        /// <inheritdoc />
        public async Task<Transaction> GetAsync(GetTransactionCriteria criteria, CancellationToken cancellationToken = default) {
            Transaction transaction = await _dbContext.Transactions
                .Include(x => x.User)
                .Include(x => x.Account)
                .Include(x => x.Period)
                .Where(x => x.Id == criteria.TransactionId)
                .Where(x => criteria.UserId == null || x.User.Id == criteria.UserId)
                .Where(x => criteria.AccountId == null || x.Account.Id == criteria.AccountId)
                .SingleOrDefaultAsync(cancellationToken);
            return transaction;
        }
    }
}