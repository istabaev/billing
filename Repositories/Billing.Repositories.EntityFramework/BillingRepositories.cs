﻿using Billing.Repositories.Accounts;
using Billing.Repositories.Transactions;
using Billing.Repositories.Users;

namespace Billing.Repositories.EntityFramework {
    /// <summary>
    /// Репозитории биллинга
    /// </summary>
    public class BillingRepositories : IBillingRepositories {
        public BillingRepositories(IUsersRepository users,
                                   IAccountsRepository accounts,
                                   ITransactionsRepository transactionsRepository) {
            Users = users;
            Accounts = accounts;
            Transactions = transactionsRepository;
        }

        /// <inheritdoc />
        public IUsersRepository Users { get; }

        /// <inheritdoc />
        public IAccountsRepository Accounts { get; }

        /// <inheritdoc />
        public ITransactionsRepository Transactions { get; }
    }
}