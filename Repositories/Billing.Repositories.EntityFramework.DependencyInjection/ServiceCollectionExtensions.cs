﻿using Billing.Repositories.Accounts;
using Billing.Repositories.EntityFramework.Accounts;
using Billing.Repositories.EntityFramework.Transactions;
using Billing.Repositories.EntityFramework.Users;
using Billing.Repositories.Transactions;
using Billing.Repositories.Users;
using Microsoft.Extensions.DependencyInjection;
using UOW;

namespace Billing.Repositories.EntityFramework.DependencyInjection {
    /// <summary>
    /// Методы расширения для регистрации репозиториев в <see cref="IServiceCollection" />
    /// </summary>
    public static class ServiceCollectionExtensions {
        /// <summary>
        /// Регистрирует репозитории в <see cref="IServiceCollection" />
        /// </summary>
        /// <param name="services">Коллекция <see cref="IServiceCollection" />, в которую добавляются сервисы</param>
        public static void AddRepositories(this IServiceCollection services) {
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IAccountsRepository, AccountsRepository>();
            services.AddScoped<ITransactionsRepository, TransactionsRepository>();
            services.AddScoped<IBillingRepositories, BillingRepositories>();
        }

        /// <summary>
        /// Регистрирует сервисы "Единицы работы" в <see cref="IServiceCollection" />
        /// </summary>
        /// <param name="services">Коллекция <see cref="IServiceCollection" />, в которую добавляются сервисы</param>
        public static void AddUnitOfWork(this IServiceCollection services) {
            services.AddScoped<IUnitOfWork<IBillingRepositories>, UnitOfWork>();
            services.AddScoped<IUnitOfWorkFactory, UnitOfWorkFactory>();
        }
    }
}