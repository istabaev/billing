﻿using System;
using Microsoft.Extensions.DependencyInjection;
using UOW;

namespace Billing.Repositories.EntityFramework.DependencyInjection {
    /// <summary>
    /// Реализация фабрики по созданию экземпляров "Единицы работы" с использованием <see cref="DependencyInjection" />
    /// </summary>
    public class UnitOfWorkFactory : IUnitOfWorkFactory {
        private readonly IServiceProvider _serviceProvider;

        public UnitOfWorkFactory(IServiceProvider serviceProvider) {
            _serviceProvider = serviceProvider;
        }

        /// <inheritdoc />
        public IUnitOfWork<TRepository> Create<TRepository>() {
            return _serviceProvider.GetService<IUnitOfWork<TRepository>>();
        }
    }
}