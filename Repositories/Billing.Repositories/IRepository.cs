﻿using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Common;

namespace Billing.Repositories {
    /// <summary>
    /// Репозиторий для работы с базой данных
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TId">Тип идентификатора сущности</typeparam>
    /// <typeparam name="TCriteria">Тип критериев поиска</typeparam>
    public interface IRepository<TEntity, in TId, in TCriteria> where TEntity : Entity<TId> {
        /// <summary>
        /// Возвращает сущность по его идентификатору
        /// </summary>
        /// <param name="criteria">Критерии поиска</param>
        /// <param name="cancellationToken">Маркер отмены задачи</param>
        /// <returns>Экземпляр сущности</returns>
        Task<TEntity> GetAsync(TCriteria criteria, CancellationToken cancellationToken = default);
    }

    /// <summary>
    /// Репозиторий с возможностью добавления новых сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип экземпляра сущности</typeparam>
    /// <typeparam name="TId">Тип идентификатора сущности</typeparam>
    public interface IAddableRepository<TEntity, in TId> where TEntity : Entity<TId> {
        /// <summary>
        /// Добавляет сущность в базу данных
        /// </summary>
        /// <param name="entity">Новая сущность</param>
        /// <param name="cancellationToken">Маркер отмены задачи</param>
        /// <returns>Экземпляр сохраненной сущности</returns>
        Task<TEntity> AddAsync(TEntity entity, CancellationToken cancellationToken = default);
    }
}