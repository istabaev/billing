﻿namespace Billing.Repositories.Accounts {
    /// <summary>
    /// Критерии поиска счета
    /// </summary>
    public class GetAccountCriteria {
        /// <summary>
        /// Идентификатор счета
        /// </summary>
        public long AccountId { get; set; }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long? UserId { get; set; }
    }
}