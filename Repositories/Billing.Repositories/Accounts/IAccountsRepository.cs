﻿using Billing.Domain.Accounts;

namespace Billing.Repositories.Accounts {
    /// <summary>
    /// Репозиторий счетов
    /// </summary>
    public interface IAccountsRepository : IRepository<Account, long, GetAccountCriteria> { }
}