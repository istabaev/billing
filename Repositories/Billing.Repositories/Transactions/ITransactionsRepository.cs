﻿using Billing.Domain.Transactions;

namespace Billing.Repositories.Transactions {
    /// <summary>
    /// Репозиторий транзакций
    /// </summary>
    public interface ITransactionsRepository : IRepository<Transaction, long, GetTransactionCriteria> { }
}