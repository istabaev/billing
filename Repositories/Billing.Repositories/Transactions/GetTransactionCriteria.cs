﻿namespace Billing.Repositories.Transactions {
    /// <summary>
    /// Критерии поиска транзакции
    /// </summary>
    public class GetTransactionCriteria {
        /// <summary>
        /// Идентификатор транзакции
        /// </summary>
        public long TransactionId { get; set; }

        /// <summary>
        /// Идентификатор счета
        /// </summary>
        public long? AccountId { get; set; }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long? UserId { get; set; }
    }
}