﻿using Billing.Repositories.Accounts;
using Billing.Repositories.Transactions;
using Billing.Repositories.Users;

namespace Billing.Repositories {
    /// <summary>
    /// Репозитории биллинга
    /// </summary>
    public interface IBillingRepositories {
        /// <summary>
        /// Репозиторий пользователей
        /// </summary>
        IUsersRepository Users { get; }

        /// <summary>
        /// Репозиторий счетов
        /// </summary>
        IAccountsRepository Accounts { get; }

        /// <summary>
        /// Репозиторий транзакций
        /// </summary>
        ITransactionsRepository Transactions { get; }
    }
}