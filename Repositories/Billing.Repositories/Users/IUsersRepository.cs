﻿using System.Threading;
using System.Threading.Tasks;
using Billing.Domain.Users;

namespace Billing.Repositories.Users {
    /// <summary>
    /// Репозиторий пользователей
    /// </summary>
    public interface IUsersRepository : IRepository<User, long, GetUserCriteria>, IAddableRepository<User, long> {
        /// <summary>
        /// Загружает список счетов пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <param name="accountIds">Список идентификаторов счетов</param>
        /// <param name="cancellationToken">Маркер отмены задачи</param>
        Task LoadAccountsAsync(User user, long[] accountIds, CancellationToken cancellationToken = default);
    }
}