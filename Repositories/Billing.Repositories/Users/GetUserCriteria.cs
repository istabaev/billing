﻿namespace Billing.Repositories.Users {
    /// <summary>
    /// Критерии поиска пользователя
    /// </summary>
    public class GetUserCriteria {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }
    }
}