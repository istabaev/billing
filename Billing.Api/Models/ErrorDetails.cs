﻿using Billing.Domain.Common.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace Billing.Api.Models {
    /// <summary>
    /// Описание ошибки
    /// </summary>
    public class ErrorDetails : ProblemDetails {
        /// <summary>
        /// Код ошибки
        /// </summary>
        public DomainErrorCode ErrorCode { get; set; }
    }
}