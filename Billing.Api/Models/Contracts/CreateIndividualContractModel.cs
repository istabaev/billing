﻿namespace Billing.Api.Models.Contracts {
    public class CreateIndividualContractModel {
        public long UserId { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }
    }
}