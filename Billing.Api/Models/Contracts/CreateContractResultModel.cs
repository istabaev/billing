﻿namespace Billing.Api.Models.Contracts {
    public class CreateContractResultModel {
        public long ContractId { get; set; }
    }
}