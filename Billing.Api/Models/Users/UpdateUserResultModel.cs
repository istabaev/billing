﻿namespace Billing.Api.Models.Users {
    public class UpdateUserResultModel {
        public bool Changed { get; set; }
    }
}