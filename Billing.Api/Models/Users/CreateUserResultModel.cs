﻿namespace Billing.Api.Models.Users {
    public class CreateUserResultModel {
        public long UserId { get; set; }
    }
}