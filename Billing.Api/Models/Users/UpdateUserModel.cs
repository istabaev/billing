﻿using Billing.Domain.Users;

namespace Billing.Api.Models.Users {
    public class UpdateUserModel {
        public UserStatus Status { get; set; }
    }
}