﻿using System;

namespace Billing.Api.Models.Users {
    public class CreateUserModel {
        public string Name { get; set; }

        public Product Product { get; set; }

        public Guid ExternalId { get; set; }
    }
}