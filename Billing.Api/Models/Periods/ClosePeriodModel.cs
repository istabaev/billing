﻿namespace Billing.Api.Models.Periods {
    public class ClosePeriodModel {
        public long UserId { get; set; }

        public long AccountId { get; set; }
    }
}