﻿namespace Billing.Api.Models.Accounts {
    public class UpdateAccountOverdraftModel {
        public long UserId { get; set; }

        public decimal Overdraft { get; set; }
    }
}