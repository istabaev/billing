﻿namespace Billing.Api.Models.Accounts {
    public class CreateAccountResultModel {
        public long AccountId { get; set; }
    }
}