﻿using Billing.Domain.Amounts;

namespace Billing.Api.Models.Accounts {
    public class CreateAccountModel {
        public long UserId { get; set; }

        public Currency Currency { get; set; }

        public string Description { get; set; }
    }
}