﻿using Billing.Domain.Accounts;

namespace Billing.Api.Models.Accounts {
    public class UpdateAccountModel {
        public long UserId { get; set; }

        public AccountStatus Status { get; set; }
    }
}