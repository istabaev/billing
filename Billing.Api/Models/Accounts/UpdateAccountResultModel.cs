﻿namespace Billing.Api.Models.Accounts {
    public class UpdateAccountResultModel {
        public bool Changed { get; set; }
    }
}