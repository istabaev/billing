﻿namespace Billing.Api.Models.Transactions {
    public class UpdateTransactionResultModel {
        public bool Changed { get; set; }
    }
}