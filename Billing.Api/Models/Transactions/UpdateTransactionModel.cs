﻿using System;
using Billing.Domain.Transactions;

namespace Billing.Api.Models.Transactions {
    public class UpdateTransactionModel {
        public long UserId { get; set; }

        public long AccountId { get; set; }

        public TransactionStatus Status { get; set; }

        public DateTime? TransactionDate { get; set; }
    }
}