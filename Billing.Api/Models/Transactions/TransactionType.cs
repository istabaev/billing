﻿namespace Billing.Api.Models.Transactions {
    public enum TransactionType {
        Payment,
        Pay,
        Transfer
    }
}