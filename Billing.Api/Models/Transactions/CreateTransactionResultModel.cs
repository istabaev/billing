﻿namespace Billing.Api.Models.Transactions {
    public class CreateTransactionResultModel {
        public long TransactionId { get; set; }
    }
}