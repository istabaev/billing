﻿namespace Billing.Api.Models.Transactions {
    public class TransferModel {
        public long UserId { get; set; }

        public long FromAccountId { get; set; }

        public long ToAccountId { get; set; }

        public decimal Amount { get; set; }

        public TransactionType TransactionType { get; set; }

        public string TransactionDescription { get; set; }
    }
}