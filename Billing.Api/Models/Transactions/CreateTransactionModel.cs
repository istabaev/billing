﻿using Billing.Domain.Transactions;

namespace Billing.Api.Models.Transactions {
    public class CreateTransactionModel {
        public long UserId { get; set; }

        public long AccountId { get; set; }

        public TransactionType Type { get; set; }

        public TransactionStatus Status { get; set; }

        public decimal Amount { get; set; }

        public string Description { get; set; }
    }
}