using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json.Serialization;
using Billing.Api.Filters;
using Billing.Commands;
using Billing.Domain.Serialization.SystemTextJson;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CQRS.Commands.DependencyInjection;
using Billing.Repositories.EntityFramework.DependencyInjection;
using Billing.EntityFramework;
using Billing.Utilities.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace Billing.Api {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        /// <summary>
        /// Application configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services) {
            services
                .AddControllers(options => { options.Filters.Add(new ExceptionFilter()); })
                .AddJsonOptions(options => {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    options.JsonSerializerOptions.Converters.Add(new EnumerationJsonConverter());
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                });

            var connectionString = Configuration.GetConnectionString("Npgsql");
            services.AddDbContext<NpgsqlBillingDbContext>(options => { options.UseNpgsql(connectionString); });
            services.AddScoped<BillingDbContext, NpgsqlBillingDbContext>();

            services.AddUtilities();
            services.AddRepositories();
            services.AddUnitOfWork();
            services.AddCommands(new[] { Assembly.GetAssembly(typeof(AsyncCommandHandler<>)) });

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new OpenApiInfo {
                    Version = "v1",
                    Title = "Billing API",
                    Description = "REST API биллинга",
                    Contact = new OpenApiContact {
                        Name = "Iskandar Tabaev",
                        Email = "istabaev@gmail.com"
                    }
                });

                c.DocInclusionPredicate((_, api) => !string.IsNullOrWhiteSpace(api.GroupName));
                c.TagActionsBy(api => new List<string> { api.GroupName });

                string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c => {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Billing API");
                    c.RoutePrefix = string.Empty;
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}