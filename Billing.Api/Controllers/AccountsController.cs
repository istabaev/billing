﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Billing.Api.Models.Accounts;
using Billing.Commands.Accounts;
using Billing.Domain.Accounts;
using CQRS.Commands;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Billing.Api.Controllers {
    /// <summary>
    /// Счета
    /// </summary>
    [ApiExplorerSettings(GroupName = "2. Счета")]
    [ApiController]
    [Route("")]
    [Produces("application/json")]
    public class AccountsController : Controller {
        private readonly ICommandExecutor _commandExecutor;

        public AccountsController(ICommandExecutor commandExecutor) {
            _commandExecutor = commandExecutor;
        }

        /// <summary>
        /// Создать счет
        /// </summary>
        /// <returns>Идентификатор созданного счета</returns>
        /// <response code="201">Возвращает идентификатор созданного счета</response>
        /// <response code="400">Произошла ошибка во время создания счета</response>  
        [HttpPost("accounts")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<CreateAccountResultModel> Create([FromBody] CreateAccountModel model,
                                                           CancellationToken cancellationToken = default) {
            var command = new CreateAccountCommand {
                UserId = model.UserId,
                Currency = model.Currency,
                Description = new AccountDescription(model.Description)
            };
            long accountId = await _commandExecutor.ExecuteAsync<CreateAccountCommand, long>(command, cancellationToken);
            return new CreateAccountResultModel { AccountId = accountId };
        }

        /// <summary>
        /// Обновить статус счета
        /// </summary>
        /// <returns>Флаг - изменились данные или нет</returns>
        /// <response code="200">Данные успешно изменены</response>
        /// <response code="400">Произошла ошибка во время выполнения</response>
        [HttpPut("accounts/{accountId:long}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<UpdateAccountResultModel> Update([FromRoute] long accountId,
                                                           [FromBody] UpdateAccountModel model,
                                                           CancellationToken cancellationToken = default) {
            bool changed;
            if (model.Status == AccountStatus.Active) {
                var command = new ActivateAccountCommand { UserId = model.UserId, AccountId = accountId };
                changed = await _commandExecutor.ExecuteAsync<ActivateAccountCommand, bool>(command, cancellationToken);
            } else if (model.Status == AccountStatus.Frozen) {
                var command = new FreezeAccountCommand { UserId = model.UserId, AccountId = accountId };
                changed = await _commandExecutor.ExecuteAsync<FreezeAccountCommand, bool>(command, cancellationToken);
            } else if (model.Status == AccountStatus.Closed) {
                var command = new CloseAccountCommand { UserId = model.UserId, AccountId = accountId };
                changed = await _commandExecutor.ExecuteAsync<CloseAccountCommand, bool>(command, cancellationToken);
            } else {
                throw new NotSupportedException();
            }

            return new UpdateAccountResultModel { Changed = changed };
        }

        /// <summary>
        /// Обновить овердрафт счета
        /// </summary>
        /// <response code="200">Данные успешно изменены</response>
        /// <response code="400">Произошла ошибка во время выполнения</response>
        [HttpPut("accounts/{accountId:long}/overdraft")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task UpdateOverdraft([FromRoute] long accountId,
                                          [FromBody] UpdateAccountOverdraftModel model,
                                          CancellationToken cancellationToken = default) {
            var command = new SetAccountOverdraftCommand {
                UserId = model.UserId,
                AccountId = accountId,
                Overdraft = new AccountOverdraft(model.Overdraft)
            };
            await _commandExecutor.ExecuteAsync(command, cancellationToken);
        }
    }
}