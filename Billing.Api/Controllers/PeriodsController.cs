﻿using System.Threading;
using System.Threading.Tasks;
using Billing.Api.Models.Periods;
using Billing.Commands.Periods;
using CQRS.Commands;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Billing.Api.Controllers {
    /// <summary>
    /// Периоды
    /// </summary>
    [ApiExplorerSettings(GroupName = "4. Периоды")]
    [ApiController]
    [Route("")]
    [Produces("application/json")]
    public class PeriodsController : Controller {
        private readonly ICommandExecutor _commandExecutor;

        public PeriodsController(ICommandExecutor commandExecutor) {
            _commandExecutor = commandExecutor;
        }

        /// <summary>
        /// Закрыть период
        /// </summary>
        /// <response code="200">Период успешно закрыт</response>
        /// <response code="400">Произошла ошибка во время закрытия</response>
        [HttpPost("periods")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task Close([FromBody] ClosePeriodModel model,
                                CancellationToken cancellationToken = default) {
            var command = new ClosePeriodCommand {
                UserId = model.UserId,
                AccountId = model.AccountId
            };
            await _commandExecutor.ExecuteAsync(command, cancellationToken);
        }
    }
}