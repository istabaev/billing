﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Billing.Api.Models.Transactions;
using Billing.Commands.Transactions;
using Billing.Domain.Amounts;
using Billing.Domain.DateTimes;
using Billing.Domain.Transactions;
using CQRS.Commands;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TransactionType = Billing.Domain.Transactions.TransactionType;

namespace Billing.Api.Controllers {
    /// <summary>
    /// Транзакции
    /// </summary>
    [ApiExplorerSettings(GroupName = "3. Транзакции")]
    [ApiController]
    [Route("")]
    [Produces("application/json")]
    public class TransactionsController : Controller {
        private readonly ICommandExecutor _commandExecutor;

        public TransactionsController(ICommandExecutor commandExecutor) {
            _commandExecutor = commandExecutor;
        }

        /// <summary>
        /// Создать транзакцию
        /// </summary>
        /// <returns>Идентификатор созданной транзакции</returns>
        /// <response code="201">Возвращает идентификатор созданной транзакции</response>
        /// <response code="400">Произошла ошибка во время создания транзакции</response>  
        [HttpPost("transactions")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<CreateTransactionResultModel> Create([FromBody] CreateTransactionModel model,
                                                               CancellationToken cancellationToken = default) {
            var command = new CreateTransactionCommand {
                UserId = model.UserId,
                AccountId = model.AccountId,
                Type = new TransactionType(model.Type.ToString("G")),
                Status = model.Status,
                Amount = new Amount(model.Amount),
                Description = new TransactionDescription(model.Description)
            };
            long transactionId = await _commandExecutor.ExecuteAsync<CreateTransactionCommand, long>(command, cancellationToken);
            return new CreateTransactionResultModel { TransactionId = transactionId };
        }

        /// <summary>
        /// Перевести средства между счетами
        /// </summary>
        /// <response code="201">Перевод успешно выполнен</response>
        /// <response code="400">Произошла ошибка во время перевода</response>  
        [HttpPost("transactions/transfer")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task Transfer([FromBody] TransferModel model,
                                   CancellationToken cancellationToken = default) {
            var command = new TransferBetweenAccountsCommand {
                UserId = model.UserId,
                FromAccountId = model.FromAccountId,
                ToAccountId = model.ToAccountId,
                Amount = new Amount(model.Amount),
                Type = new TransactionType(model.TransactionType.ToString("G")),
                Description = new TransactionDescription(model.TransactionDescription)
            };
            await _commandExecutor.ExecuteAsync(command, cancellationToken);
        }

        /// <summary>
        /// Обновить статус транзакции
        /// </summary>
        /// <returns>Флаг - изменились данные или нет</returns>
        /// <response code="200">Данные успешно изменены</response>
        /// <response code="400">Произошла ошибка во время выполнения</response>
        [HttpPut("transactions/{transactionId:long}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<UpdateTransactionResultModel> Update([FromRoute] long transactionId,
                                                               [FromBody] UpdateTransactionModel model,
                                                               CancellationToken cancellationToken = default) {
            bool changed;
            if (model.Status == TransactionStatus.Accepted) {
                var command = new AcceptTransactionCommand {
                    UserId = model.UserId,
                    AccountId = model.AccountId,
                    TransactionId = transactionId,
                    TransactionDate = model.TransactionDate?.ToDate()
                };
                changed = await _commandExecutor.ExecuteAsync<AcceptTransactionCommand, bool>(command, cancellationToken);
            } else if (model.Status == TransactionStatus.Cancelled) {
                var command = new CancelTransactionCommand {
                    UserId = model.UserId,
                    AccountId = model.AccountId,
                    TransactionId = transactionId
                };
                changed = await _commandExecutor.ExecuteAsync<CancelTransactionCommand, bool>(command, cancellationToken);
            } else if (model.Status == TransactionStatus.Reserved) {
                var command = new ReserveTransactionCommand {
                    UserId = model.UserId,
                    AccountId = model.AccountId,
                    TransactionId = transactionId
                };
                changed = await _commandExecutor.ExecuteAsync<ReserveTransactionCommand, bool>(command, cancellationToken);
            } else {
                throw new NotSupportedException();
            }

            return new UpdateTransactionResultModel { Changed = changed };
        }
    }
}