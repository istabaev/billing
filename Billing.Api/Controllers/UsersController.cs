﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Billing.Api.Models.Users;
using Billing.Commands.Users;
using Billing.Domain.Users;
using CQRS.Commands;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Product = Billing.Domain.Users.Product;

namespace Billing.Api.Controllers {
    /// <summary>
    /// Пользователи
    /// </summary>
    [ApiExplorerSettings(GroupName = "1. Пользователи")]
    [ApiController]
    [Route("")]
    [Produces("application/json")]
    public class UsersController : Controller {
        private readonly ICommandExecutor _commandExecutor;

        public UsersController(ICommandExecutor commandExecutor) {
            _commandExecutor = commandExecutor;
        }

        /// <summary>
        /// Создать пользователя
        /// </summary>
        /// <returns>Идентификатор созданного пользователя</returns>
        /// <response code="201">Возвращает идентификатор созданного пользователя</response>
        /// <response code="400">Произошла ошибка во время создания пользователя</response>  
        [HttpPost("users")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<CreateUserResultModel> Create([FromBody] CreateUserModel model,
                                                        CancellationToken cancellationToken = default) {
            var command = new CreateUserCommand {
                Name = new UserName(model.Name),
                ExternalUser = new ExternalUser(new Product(model.Product.ToString("G")), new ExternalId(model.ExternalId.ToString("D")))
            };
            long userId = await _commandExecutor.ExecuteAsync<CreateUserCommand, long>(command, cancellationToken);
            return new CreateUserResultModel { UserId = userId };
        }

        /// <summary>
        /// Обновить статус пользователя
        /// </summary>
        /// <returns>Флаг - изменились данные или нет</returns>
        /// <response code="200">Данные успешно изменены</response>
        /// <response code="400">Произошла ошибка во время выполнения</response>  
        [HttpPut("users/{userId:long}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<UpdateUserResultModel> Update([FromRoute] long userId,
                                                        [FromBody] UpdateUserModel model,
                                                        CancellationToken cancellationToken = default) {
            bool changed;
            if (model.Status == UserStatus.Active) {
                var command = new ActivateUserCommand { UserId = userId };
                changed = await _commandExecutor.ExecuteAsync<ActivateUserCommand, bool>(command, cancellationToken);
            } else if (model.Status == UserStatus.Blocked) {
                var command = new BlockUserCommand { UserId = userId };
                changed = await _commandExecutor.ExecuteAsync<BlockUserCommand, bool>(command, cancellationToken);
            } else {
                throw new NotSupportedException();
            }

            return new UpdateUserResultModel { Changed = changed };
        }
    }
}