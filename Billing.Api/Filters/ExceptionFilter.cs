﻿using Billing.Api.Models;
using Billing.Domain.Common.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Billing.Api.Filters {
    /// <summary>
    /// Фильтр для изменения ответа при исключениях
    /// </summary>
    public class ExceptionFilter : IActionFilter, IOrderedFilter {
        /// <inheritdoc />
        public int Order => int.MaxValue - 10;

        /// <inheritdoc />
        public void OnActionExecuted(ActionExecutedContext context) {
            if (context.Exception is DomainException domainException) {
                var error = new ErrorDetails {
                    Status = StatusCodes.Status400BadRequest,
                    Title = domainException.Message,
                    ErrorCode = domainException.Code
                };
                context.Result = new ObjectResult(error) {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }
        }

        /// <inheritdoc />
        public void OnActionExecuting(ActionExecutingContext context) { }
    }
}